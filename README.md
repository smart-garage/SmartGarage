# GarageQL

Hello! Welcome to our application!

## Description:
A single-page application that provides an easy to work with system for employees of an auto-repair shop to track their daily work related to vehicles and customers.

Customers can check what repairs and services have been provided, how much they cost.

This is a student project, part of the final assignment of the Telerik Alpha JavaScript track.

## Creators:
Ivelin Stoyanov - [@Ivelin.Stoyanov](https://gitlab.com/Ivelin.Stoyanov) <br>
Ralitza Stratieva - [@ralitzastratieva](https://gitlab.com/ralitzastratieva)

## Core Back-end Technologies:
- Node.js
- MariaDB
- TypeORM
- NestJS
- GraphQL
- Apollo Server
- Redis

## Core Front-end Technologies:
- React
- Material-UI

# Features

## For customers
- Forgotten password option - Customers can request an e-mail which provides a link for password reset, where they can set a new password, in case they have forgotten it. Redis expires the token for the password reset after 1 day.
Here is the flow in screenshots:
<br>
![Login screen](https://gitlab.com/smart-garage/SmartGarage/-/raw/31-add-screenshots-to-readme/readme-images/login-1.png)
![Forgotten password screen](https://gitlab.com/smart-garage/SmartGarage/-/raw/31-add-screenshots-to-readme/readme-images/login-2.png)
![E-mail for reset sent](https://gitlab.com/smart-garage/SmartGarage/-/raw/31-add-screenshots-to-readme/readme-images/login-3.png)
![E-mail for reset](https://gitlab.com/smart-garage/SmartGarage/-/raw/31-add-screenshots-to-readme/readme-images/login-4.png)
![Resetting password](https://gitlab.com/smart-garage/SmartGarage/-/raw/31-add-screenshots-to-readme/readme-images/login-5.png)
![Success](https://gitlab.com/smart-garage/SmartGarage/-/raw/31-add-screenshots-to-readme/readme-images/login-6.png)

## For employees
- Employees can:
- create (register) new customers, edit already existing ones.
- create new vehicles
  Here is the flow for registering a customer, vehicle and creating a report with services for them:

## Prerequisites
 - [Node.js](https://nodejs.org/en/) installation
 - [MariaDB](https://mariadb.org/download/) installation

## Redis
You will need to install Redis, if you don't have it yet. 
Follow the link below for a more detailed explanation and download Redis-x64-3.2.100.zip.
Unzip the folder, run redis-server.exe and you're good to start up the server.

 - [Redis](https://riptutorial.com/redis/example/29962/installing-and-running-redis-server-on-windows)

# Starting the server

Create a **.env** file with the following information:<br>

DBTYPE=mariadb<br>
HOST=localhost<br>
DBPORT=3306<br>
PORT=5555<br>
USERNAME=root<br>
PASSWORD=toot<br>
DATABASE=test_2<br>
PRIVATE_KEY=k3yT1me<br>
TOKEN_LIFETIME=1h

You can run the server with **'nest start'** or **'nest start --watch'** if you would like to make changes to the server and see them reflected immediately after saving the file.

# Running server unit tests
**npm run test** or <br>
**npm run test:watch**

# Database
We've uploaded an SQL dump, so that you can test the application without having to manually type in data. The dump is located in **SmartGarage\SQL-dump**.

Here is a visualization of the database relations:
![Database relations](https://gitlab.com/smart-garage/SmartGarage/-/raw/31-add-screenshots-to-readme/readme-images/database-relations.PNG)

# GraphQL Playground
A cool feature you can use to test the server and http://localhost:5555/graphql
An example request for creating an admin user is below. We removed the authentication guard for creating an employee so that you can create your own. You will receive an e-mail with a randomly generated password and then you can log in at **/login/employee**.
![Create employee mutation](https://gitlab.com/smart-garage/SmartGarage/-/raw/31-add-screenshots-to-readme/readme-images/gql-playground.PNG)

# Starting the client
You can run the client with **'npm start'**. It should start up at http://localhost:3000/



# Features planned for the next sprint:
- Implementing the 'confirm account' feature that enables the user to log in, after confirming by clicking on the link in the automatically generated registartion e-mail.
- Implement a currency converting option for the services and reports.
- Blacklist or whitelist tokens with Redis.
- Implement better error handling on the frontend.

Thank you for the attention!
