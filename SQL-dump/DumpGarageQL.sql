-- MariaDB dump 10.19  Distrib 10.5.9-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: garageql
-- ------------------------------------------------------
-- Server version	10.5.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) NOT NULL DEFAULT 'regularUser',
  `email` varchar(255) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT 0,
  `phone` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_fdb2f3ad8115da4c7718109a6e` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,'regularUser','ivan@abv.bg','Ivan','Stoimenov','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0888888882'),(2,'regularUser','ralitza.stratieva.a27@learn.telerikacademy.com','Ralitza','Stratieva','$2b$10$46z8wZO827nyh6m4ngWBauO3jyvpqLeZ5NsTXkelR7wtdiWfluf4G',0,'0887399994'),(3,'regularUser','lolete2361@vvaa1.com','Lola','Shopova','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0887378994'),(4,'regularUser','edgar97@emmy.de','Edgar','Poe','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0888444932'),(5,'regularUser','toddwashere@nav.com','Todd','Washere','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0888675394'),(6,'regularUser','lonagar@gamel.com','Ylona','Garcia','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0883378994'),(7,'regularUser','gummy@abv.bg','Arabella','Evans','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0877371732'),(8,'regularUser','ivelina@abv.bg','Ivelina','Stanoeva','$10$FAa6a14lwfZmx8Zaweok3eT.XfaSpaghetCe4uAZsI6XYqq5iViD7Yy',1,'0889678838'),(9,'regularUser','robhodan@mail.com','Robert','Hodan','$231gdfasfaghAAseT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0887918994'),(10,'regularUser','neillfontes@neil.be','Neil','Fontes','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0888129300'),(11,'regularUser','maleeva@north.de','Sabina','Maleeva','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losdse4uAZsI6XYqq5iViD7Yy',1,'0876337899'),(12,'regularUser','rvmladenov@modis.bg','Rostislav','Mladenov','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0877672638'),(13,'regularUser','RubenMateus@mateu.se','Ruben','Mateus','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0887378991'),(14,'regularUser','narlicer@sub.de','Stephan','Narayon','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0878321732'),(15,'regularUser','nickrodgers42@sb.com','Nick','Rodgers','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0877672411'),(16,'regularUser','ztolive@oliveer.se','Zob','Olive','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0883278994'),(17,'regularUser','zenaidagonzalez@niki.com','Zenaida','Gonzalez','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0888129455'),(18,'regularUser','koseva@abv.bg','Elena','Koseva','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0881678838'),(19,'regularUser','maneva@ddd.de','Diana','Maneva','$20$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0877678823'),(20,'regularUser','ganevska@gmail.com','Isabel','Ganevska','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0898378994'),(21,'regularUser','dolev@artevent.com','Dimitar','Dolev','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0888494932'),(22,'regularUser','3dberg@api.bg','Sinba','Rauschenberg','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0887399995'),(23,'regularUser','m.stoffels@gmail.com','Mitch','Stoffels','$20$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0888129452'),(24,'regularUser','adv.ved@abv.bg','Simeon','Pantikov','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0878321347'),(25,'regularUser','mirka@mirc.bg','Mira','Saraneva','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0898332194'),(26,'regularUser','kumova@dds.bg','Anotniya','Kumova','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0877371119'),(27,'regularUser','bengalska@dds.bg','Gergana','Bengalska','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0877672512'),(28,'regularUser','mih@gmail.com','Stoyan','Mihailov','$20$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0877471732'),(29,'regularUser','ivanov@gmail.com','Danail','Ivanov','$20$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0888129477'),(30,'regularUser','marinkov@md.bg','Konstantin','Marinov','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0877678812'),(31,'regularUser','dr.musano@mail.bg','Drake','Musano','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0889217648'),(32,'regularUser','mrsmart@yahoo.com','Melanie','Martinez','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0877371122'),(33,'regularUser','carey@ind.nl','Marshall','Carey','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0887399962'),(34,'regularUser','iv.manolov@cando.bg','Ivomir','Manolov','$10$FAa6a14lwfZmx8Zaweok3eT.Xfau7losCe4uAZsI6XYqq5iViD7Yy',1,'0888129400');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) NOT NULL DEFAULT 'employee',
  `email` varchar(255) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_817d1d427138772d47eca04885` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (1,'employee','ralitza.stratieva.a27@learn.telerikacademy.com','Ralitza','Stratieva','$2b$10$kfqLF2D3MIfOVYJifMSKQO..8l6hdpiuxbfRMZmYLuQsUYbVvRXg6'),(2,'employee','ivelinQL@abv.bg','Ivelin','Stoyanov','$10$kfqLF2D3MIfOVYJifMSKQO..8l6hdpiuxbfRMZmYLuQsUYbVvRXg6'),(3,'employee','samiam@drseuss.com','Sam','Higgins','$10$kfqLF2D3MIfOVYJifMSKQO..8l6hdpiuxbfRMZmYLuQsUYbVvRXg6');
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime(6) NOT NULL DEFAULT current_timestamp(6),
  `vehicleId` int(11) NOT NULL,
  `customerId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_59d3ad44a823b6b236d0385a602` (`vehicleId`),
  KEY `FK_af0ab438a1e8dc4878cdc44948e` (`customerId`),
  CONSTRAINT `FK_59d3ad44a823b6b236d0385a602` FOREIGN KEY (`vehicleId`) REFERENCES `vehicle` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_af0ab438a1e8dc4878cdc44948e` FOREIGN KEY (`customerId`) REFERENCES `customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report`
--

LOCK TABLES `report` WRITE;
/*!40000 ALTER TABLE `report` DISABLE KEYS */;
INSERT INTO `report` VALUES (1,'2019-09-12 00:00:00.000000',1,4),(2,'2019-10-11 00:00:00.000000',2,4),(3,'2020-02-23 00:00:00.000000',3,1),(4,'2021-05-10 00:00:00.000000',4,5),(5,'2020-01-31 00:00:00.000000',5,2),(6,'2019-06-07 00:00:00.000000',6,7),(8,'2020-10-03 00:00:00.000000',11,1),(9,'2021-03-10 00:00:00.000000',23,20),(10,'2021-04-16 00:00:00.000000',24,21),(11,'2021-04-17 00:00:00.000000',12,12),(12,'2021-08-23 00:00:00.000000',2,4),(13,'2021-07-03 00:00:00.000000',17,13),(14,'2021-08-11 00:00:00.000000',10,10),(15,'2021-12-10 00:00:00.000000',11,1),(16,'2020-07-12 00:00:00.000000',27,23),(17,'2021-03-12 00:00:00.000000',15,3),(18,'2019-09-10 00:00:00.000000',15,3),(19,'2020-11-11 00:00:00.000000',9,8),(20,'2019-12-09 00:00:00.000000',31,27);
/*!40000 ALTER TABLE `report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_services_service`
--

DROP TABLE IF EXISTS `report_services_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_services_service` (
  `reportId` int(11) NOT NULL,
  `serviceId` int(11) NOT NULL,
  PRIMARY KEY (`reportId`,`serviceId`),
  KEY `IDX_21c6a9929c86c44ea779319383` (`reportId`),
  KEY `IDX_e3eeef5a99c26ed447ad7697e2` (`serviceId`),
  CONSTRAINT `FK_21c6a9929c86c44ea7793193839` FOREIGN KEY (`reportId`) REFERENCES `report` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_e3eeef5a99c26ed447ad7697e29` FOREIGN KEY (`serviceId`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_services_service`
--

LOCK TABLES `report_services_service` WRITE;
/*!40000 ALTER TABLE `report_services_service` DISABLE KEYS */;
INSERT INTO `report_services_service` VALUES (1,8),(1,10),(2,1),(2,2),(2,4),(3,19),(4,2),(5,3),(6,1),(7,15),(8,16),(9,17),(10,13),(11,2),(12,9),(13,8),(14,3),(15,4),(16,14),(16,17),(17,2),(18,4),(19,1),(19,6),(20,2),(20,19);
/*!40000 ALTER TABLE `report_services_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (1,'Front End Alignment',120),(2,'Four Wheel Alignment',150),(3,'Fuel Induction Service',300),(4,'Repaint',400),(5,'Wheel Alignment Check',15),(6,'Steering Wheel Inspection',50),(7,'Air Conditioning Maintenance',120),(8,'Light Bulb Change',98),(9,'Bumper Replacement',230),(10,'Filters',100),(11,'Brake Fluid Flush',75),(12,'Window Tint',400),(13,'Windshield Replacement',700),(14,'Oil Change',210),(15,'Fuel Injection Service',200),(16,'Minor Crash Repair',130),(17,'Transmission Service',200),(18,'Audio, GPS, Bluetooth',300),(19,'Auto Reconditioning',1000),(20,'Battery Charging',100),(21,'Exhaust System Repair',200);
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicle`
--

DROP TABLE IF EXISTS `vehicle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturer` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `year` varchar(255) NOT NULL,
  `vin` varchar(255) NOT NULL,
  `registration_plate` varchar(255) NOT NULL,
  `customerId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_5926d02d0b2a47a920a044b0984` (`customerId`),
  CONSTRAINT `FK_5926d02d0b2a47a920a044b0984` FOREIGN KEY (`customerId`) REFERENCES `customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicle`
--

LOCK TABLES `vehicle` WRITE;
/*!40000 ALTER TABLE `vehicle` DISABLE KEYS */;
INSERT INTO `vehicle` VALUES (1,'Renault','Captur','2000','JM1BL1V70C1537364','CA1122BB',4),(2,'Renault','Clio','2020','JT2CE82L9F3006626','CA4775KT',4),(3,'Renault','Espace','2018','1FTFX1E57JKE37092','OB4444AP',1),(4,'Renault','Express','2020','LJCPCBLCX11000237','B7997AB',5),(5,'Renault','Fluence','2019','1HFSC3404VA000123','CA4325KT',2),(6,'Renault','Grand Espace','2017','JH2PC35051M200020','E2043KB',7),(7,'Renault','Grand Modus','2007','XTA210990Y2766389','CA1063PB',9),(8,'Renault','Wind','2001','JZ4423BEY12914NMJ','PB4282KC',6),(9,'Opel','Agila','1999','WAUZZZ8A9MA123519','PB7158TC',8),(10,'Opel','Agila','2008','MZ20418ADSFJE2817','PB6243KC',10),(11,'Opel','Vectra','2020','RO0928197UAJFK932','CB6606MA',1),(12,'Opel','Astra caravan','2007','JT2CESDF9F3006626','E4386KB',12),(13,'Opel','Movano','2014','XTA210800P1365910','EH7282BA',14),(14,'Opel','Omega','2015','2SFSC3404VA000123','PB9423BH',1),(15,'Opel','Signum','2000','JH2PC38851M200020','TX2483BX',3),(16,'Opel','Zafira','2005','WAUAZZ8A9ND123529','CA7622BB',11),(17,'Subaru','Forester','2008','LTCPCBLCX11011237','B7897AB',13),(18,'Subaru','Impreza','2020','RO884348VJZH80021','CA1417PB',15),(19,'Subaru','Justy','2005','WBGZZZ8A9MA123512','E4696KD',16),(20,'Subaru','Legacy','2000','JT2CE82L9F7656626','CB3994KB',17),(21,'Subaru','Levorg','2018','JM1BL1V70C1537355','CB6037KC',19),(22,'Subaru','Outback','2011','MZ20418AAAFJE2817','CA2088PB',18),(23,'Subaru','SVX','2011','JT2NESDF9F3006511','CA8605CB',20),(24,'Subaru','Tribeca','2000','RO994348VJZH81021','E2023KB',21),(25,'Toyota','4-Runner','2005','1FTFX1E57JKE37931','CA1077PB',22),(26,'Toyota','Auris','2007','GO884348SAZH80021','PB5181KC',3),(27,'Toyota','Auris','2000','MZ40418ASSFJE2817','PB7512TC',23),(28,'Toyota','Aygo','2010','JNRAS08W83X204595','CB6677MA',24),(29,'Toyota','Camry','2015','Z8NAHJKL029185738','E4396KB',26),(30,'Toyota','Corolla','2001','NLA8918FB2188DMAN','EH7392BA',25),(31,'Toyota','FJ Cruiser','2010','1HGBH3195XJMOA948','PB9427BH',27),(32,'Toyota','GT86','2011','AV29FAJ021JAJLA99','TX2403BX',28),(33,'Toyota','Hiace','2020','NAV67523HTE91WKND','CB3955KB',29),(34,'Hummer','H2','2020','1FDFX1E57JKE37092','CB7137KC',30),(35,'Hummer','H3','2014','JT2NKADF9F3006511','CA3121PB',2),(36,'Hyundai','Accent','2015','MZ20448ABAFJ42817','CA7705CB',3),(37,'Hyundai','Elantra','2018','FG20418OAAFJE2817','OB0444AP',31),(38,'Hyundai','Galloper','2017','JH2PM35051M200020','PB6128PA',32),(39,'Rover','200','2017','LAVPCBLCX11011237','CA2344PB',33),(40,'Rover','75','2014','WAUBJ48A9MA123519','B7832AB',34);
/*!40000 ALTER TABLE `vehicle` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-09 23:04:19
