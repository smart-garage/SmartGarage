import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CustomersModule } from './customers/customers.module';
import { EmployeesModule } from './employees/employees.module';
import { VehiclesModule } from './vehicles/vehicles.module';
import { ServicesModule } from './services/services.module';
import { ReportsModule } from './reports/reports.module';
import * as dotenv from 'dotenv';
import { GraphQLError, GraphQLFormattedError } from 'graphql';
import { AuthModule } from './auth/auth.module';

const config = dotenv.config().parsed;

@Module({
  imports: [
    GraphQLModule.forRoot({
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
      formatError: (error: GraphQLError) => {
        // error when a required field is missing from the mutation
        if (error.name === 'ValidationError') {
          return new Error(error.message);
        }

        // temp fix to internal server error problem
        if (!error.extensions.exception.response) {
          // alternatively replace the mssage with Unexpected error!
          return new Error(error.message);
        }

        const graphQLFormattedError: GraphQLFormattedError = {
          message: error.extensions.exception.response.message || error.message,
          extensions: { statusCode: error.extensions.exception.status }, // might leave that out
        };
        return graphQLFormattedError;
      },
    }),
    TypeOrmModule.forRoot({
      type: 'mariadb',
      host: config.HOST,
      port: +config.DBPORT,
      username: config.USERNAME,
      password: config.PASSWORD,
      database: config.DATABASE,
      synchronize: true,
      logging: true,
      entities: [join(__dirname, '**', '*.entity.{ts,js}')],
      migrations: ['src/migration/**/*.ts'],
      subscribers: ['src/subscriber/**/*.ts'],
      cli: {
        entitiesDir: 'src/entities',
        migrationsDir: 'src/migration',
        subscribersDir: 'src/subscriber',
      },
    }),
    CustomersModule,
    EmployeesModule,
    VehiclesModule,
    ServicesModule,
    ReportsModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
