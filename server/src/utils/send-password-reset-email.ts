import * as nodemailer from 'nodemailer';
import * as handlebars from 'handlebars';
import * as fs from 'fs';
import * as path from 'path';

const transporter = nodemailer.createTransport({
  service: 'gmail',
  port: 587,
  secure: false, // true for 465, false for other ports
  auth: {
    user: 'smartgaragebot@gmail.com',
    pass: 'heyboys222',
  },
  tls: {
    rejectUnauthorized: false, // to allow authorization
  },
});

export const sendPassResetEmail = async function (email: string, url: string) {
  const filePath = path.join(__dirname, './e-mail-templates/password-reset/password-reset.html');
  const source = fs.readFileSync(filePath, 'utf-8').toString();
  const template = handlebars.compile(source);
  const replacements = {
    username: email,
    url
  };
  const htmlToSend = template(replacements);
  const mailOptions = await transporter.sendMail({
    from: '"GarageQL" <smartgaragebot@gmail.com>',
    to: email,
    subject: 'Reset your password',
    text: `A password reset for GarageQL has been requested! 
    Click here to reset your password - ${url}`,
    html: htmlToSend,
  });

  console.log('Message sent: %s', mailOptions.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
};
