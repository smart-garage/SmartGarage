import { redis } from 'src/redis';
import * as uuid from 'uuid'; // version 4 for creating a unique id

export const createPassResetUrl = async (customerId: number) => {
  // create a token associated with the user's ID that we send to the server
  // to confirm the account
  const token = uuid.v4(); // version 4 of UUID
  await redis.set(token, customerId, 'EX', 60 * 60 * 24); // 1 day, then redis expires the token

  // return unique URL
  return `http://localhost:3000/forgotten-password-reset?token=${token}`; // to frontend
};
