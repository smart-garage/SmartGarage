import { InputType, Int, Field } from '@nestjs/graphql';
import { IsEmail, Length } from 'class-validator';

@InputType()
export class CreateEmployeeInput {
  @Field()
  @IsEmail()
  @Length(1, 60, { message: 'E-mail must be between 2 and 60 characters!' })
  email: string;

  @Field()
  @Length(2, 20, { message: 'Name must be between 2 and 20 characters!' })
  firstName: string;

  @Field()
  @Length(2, 20, { message: 'Name must be between 2 and 20 characters!' })
  lastName: string;
}
