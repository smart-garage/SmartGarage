import { Resolver, Query, Mutation, Args, Int } from '@nestjs/graphql';
import { EmployeesService } from './employees.service';
import { Employee } from './entities/employee.entity';
import { CreateEmployeeInput } from './dto/create-employee.input';
import { UpdateEmployeeInput } from './dto/update-employee.input';
import { AuthService } from 'src/auth/auth.service';
import { LoginInput } from 'src/auth/dto/login.input';
import { UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { redis } from 'src/redis';
import { ChangeForgottenPasswordInput } from 'src/auth/dto/change-forgotten-password.input';
import { createPassResetUrl } from 'src/utils/create-pass-reset-url';
import { sendPassResetEmail } from 'src/utils/send-password-reset-email';

@Resolver(() => Employee)
export class EmployeesResolver {
  constructor(private readonly employeesService: EmployeesService,
    private readonly authService: AuthService) { }

  // Get all employees
  @UseGuards(JwtAuthGuard)
  @Query(() => [Employee])
  employees(): Promise<Employee[]> {
    return this.employeesService.findAll();
  }

  // Get one employee by id
  @UseGuards(JwtAuthGuard)
  @Query(() => Employee)
  employeeById(@Args('id', { type: () => Int }) id: number) {
    return this.employeesService.findOne(id);
  }

  // Get one employee by e-mail
  @UseGuards(JwtAuthGuard)
  @Query(() => Employee)
  employeeByMail(@Args('email', { type: () => String }) email: string) {
    return this.employeesService.findOneByMail(email);
  }

  // Create employee
  // @UseGuards(JwtAuthGuard)
  @Mutation(() => Employee)
  createEmployee(
    @Args('createEmployeeInput') createEmployeeInput: CreateEmployeeInput,
  ) {
    return this.employeesService.createEmployee(createEmployeeInput);
  }

  // Update employee
  @UseGuards(JwtAuthGuard)
  @Mutation(() => Employee)
  updateEmployee(
    @Args('id', { type: () => Int }) id: number,
    @Args('updateEmployeeInput') updateEmployeeInput: UpdateEmployeeInput,
  ) {
    return this.employeesService.updateEmployee(id, updateEmployeeInput);
  }

  // Delete employee
  @UseGuards(JwtAuthGuard)
  @Mutation(() => Employee)
  removeEmployee(@Args('id', { type: () => Int }) id: number) {
    return this.employeesService.deleteEmployee(id);
  }

  // Employee login
  @Mutation(() => String)
  async loginEmployee(@Args('loginInput') loginInput: LoginInput) {
    const { email, password } = loginInput;

    const user = await this.employeesService.findOneByMail(email); // check if this doesn't throw..
    if (!user) {
      return 'Invalid credentials!' // or throw new Error()?
    }

    const validPass = await this.authService.validatePassword(user, password);
    if (!validPass) {
      return 'Invalid credentials!' // or throw new Error()?
    }

    const access_token = (await this.authService.CreateAccessToken(user)).access_token;
    // or in an object if needed
    return access_token;
  }

  // Change password
  @UseGuards(JwtAuthGuard)
  @Mutation(() => String)
  async changePasswordEmployee(
    @Args('id', { type: () => Int }) id: number,
    @Args('password', { type: () => String }) password: string,
  ) {
    await this.employeesService.changePassword(id, password);
    // expire token?
    return 'Password changed!';
  }

  // Forgotten password
  // @UseGuards(JwtAuthGuard)
  @Mutation(() => String)
  async forgotPasswordEmployee(@Args('email') email: string): Promise<string> {
    const customer = await this.employeesService.findOneByMail(email);
    await sendPassResetEmail(email, await createPassResetUrl(customer.id));

    return 'Password reset e-mail sent!';
  }

    // Change forgotten password
    @Mutation(() => String)
    async changeForgottenPasswordEmployee(
      @Args('changePasswordInput') { token, password }: ChangeForgottenPasswordInput,
    ) {
      const employeeId = await redis.get(token);
  
      if (!employeeId) {
        return 'Something went wrong!';
      }
      await this.employeesService.changePassword(+employeeId, password);
      // expire token?
      return 'Password changed!';
    }

}
