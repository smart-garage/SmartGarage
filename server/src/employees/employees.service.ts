import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateEmployeeInput } from './dto/create-employee.input';
import { UpdateEmployeeInput } from './dto/update-employee.input';
import { Employee } from './entities/employee.entity';
import * as bcrypt from 'bcrypt';
import { generatePassword } from 'src/auth/password-generator';
import { createConfirmationUrl } from 'src/utils/create-confirmation-url';
import { sendRegistrationEmail } from 'src/utils/send-registration-email';

const saltRounds = 10;

@Injectable()
export class EmployeesService {
  constructor(
    @InjectRepository(Employee)
    private employeesRepository: Repository<Employee>
  ) { }

  // Get all employees
  async findAll() {
    return await this.employeesRepository.find();
  }

  // Get one employee by id
  async findOne(id: number): Promise<Employee> {
    return await this.employeesRepository.findOneOrFail(id);  
  }

  // Get one employee by e-mail
  async findOneByMail(email: string): Promise<Employee> {
    return await this.employeesRepository.findOneOrFail({ where: { email } });
  }

  //  Create an employee
  async createEmployee(
    createEmployeeInput: CreateEmployeeInput
    ): Promise<Employee> {
      const { email } = createEmployeeInput;
      const password = generatePassword();

      const existingCheck = await this.employeesRepository.findOne({ where: { email } });
      if (existingCheck) {
        throw new Error('E-mail already in the system!');
      }
      const hashedPassword = await bcrypt.hash(password, saltRounds);
  
      const newEmployee = this.employeesRepository.create({
        password: hashedPassword,
        ...createEmployeeInput,
      });
      // send pass and confirmation URL per mail
      await sendRegistrationEmail(
        email,
        password,
        await createConfirmationUrl(+newEmployee.id),
      );
  
      return await this.employeesRepository.save({
        ...newEmployee,
        hashedPassword,
      });
  }

  // Update an employee
  async updateEmployee(id: number, updateEmployeeInput: UpdateEmployeeInput): Promise<Employee> {
    await this.employeesRepository.findOneOrFail(id);
    await this.employeesRepository.save({ id, ...updateEmployeeInput });

    return await this.employeesRepository.findOne(id);
  }

  // Delete employee by id
  async deleteEmployee(id: number): Promise<Employee> {
    const deletedEmployee = await this.employeesRepository.findOneOrFail(id);
    await this.employeesRepository.delete(id);

    return deletedEmployee;
  }

  // Change password
  async changePassword(id: number, password: string) {
    const hashedPassword = await bcrypt.hash(password, saltRounds);
    await this.employeesRepository.update(id, {
      password: hashedPassword,
    });
  }
}
