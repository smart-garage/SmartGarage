import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from 'src/auth/auth.service';
import { EmployeesResolver } from './employees.resolver';
import { EmployeesService } from './employees.service';

describe('EmployeesResolver', () => {
  let resolver: EmployeesResolver;

  const mockEmployees: any[] = [
    {
      id: 1,
      firstName: 'Alissa',
      lastName: 'Rhymes',
      password: '$2b$10$zHKl5PTh3Vx79DCJisnPw.UF0jOWzfMm0jBtrNEXtrsO9Gdg',
      email: 'alyss@abv.bg',
    },
    {
      id: 2,
      firstName: 'Ivelin',
      lastName: 'QL',
      password: '$2b$10$zHKl5PTh3Vx79DCJisnPw.UF0jOWzfMm0jBtrNEXtrsO9Gdg',
      email: 'ivelincho@dollars.com',
    },
    {
      id: 3,
      firstName: 'Toni',
      lastName: 'Bonboni',
      password: '$2b$10$zHKl5PTh3Vx79DCJisnPw.UF0jOWzfMm0jBtrNEXtrsO9Gdg',
      email: 'antoan@qua.bg',
    },
    {
      id: 4,
      firstName: 'Twin',
      lastName: 'Barnes',
      password: '$2b$10$zHKl5PTh3Vx79DCJisnPw.UF0jOWzfMm0jBtrNEXtrsO9Gdg',
      email: 'alyss@abv.bg',
    },
  ];
  
  const mockEmployeesService = {
    findAll: jest.fn(() => mockEmployees),
    findOne: jest.fn(
      (id) => mockEmployees.filter((vehicle) => vehicle.id === id)[0],
    ),
    updateEmployee: jest.fn((id, dto) => {
      return {
        id,
        ...dto,
      };
    }),
    deleteEmployee: jest.fn((id) => {
      mockEmployees.splice(id - 1, 1);
    }),
  };

  const mockAuthService = {};

  const updateToEmployee = {
    firstName: 'Alissandra',
  }

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EmployeesResolver, EmployeesService, AuthService],
    })
    .overrideProvider(EmployeesService)
    .useValue(mockEmployeesService)
    .overrideProvider(AuthService)
    .useValue(mockAuthService)
    .compile();

    resolver = module.get<EmployeesResolver>(EmployeesResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });

  it('should return all employees', async () => {
    expect(resolver.employees()).toEqual(mockEmployees);
  });

  it('should return a single employee', async () => {
    expect(resolver.employeeById(1)).toEqual(mockEmployees[0]);
  });

  it('should update an employee', async () => {
    const initialCustomerFirstName = mockEmployees[0].firstName;
    const newFirstName = (await resolver.updateEmployee(1, updateToEmployee))
      .firstName;
    expect(initialCustomerFirstName).not.toEqual(newFirstName);
  });

  it('should delete an employee', async () => {
    const initialLength = mockEmployees.length;
    await resolver.removeEmployee(initialLength - 1);
    expect(mockEmployees.length).toEqual(initialLength - 1);
  });
});
