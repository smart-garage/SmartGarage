import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { EmployeesService } from './employees.service';
import { Employee } from './entities/employee.entity';

describe('EmployeesService', () => {
  let service: EmployeesService;

  const mockEmployees: any[] = [
    {
      id: 1,
      firstName: 'Alissa',
      lastName: 'Rhymes',
      password: '$2b$10$zHKl5PTh3Vx79DCJisnPw.UF0jOWzfMm0jBtrNEXtrsO9Gdg',
      email: 'alyss@abv.bg',
    },
    {
      id: 2,
      firstName: 'Ivelin',
      lastName: 'QL',
      password: '$2b$10$zHKl5PTh3Vx79DCJisnPw.UF0jOWzfMm0jBtrNEXtrsO9Gdg',
      email: 'ivelincho@dollars.com',
    },
    {
      id: 3,
      firstName: 'Toni',
      lastName: 'Bonboni',
      password: '$2b$10$zHKl5PTh3Vx79DCJisnPw.UF0jOWzfMm0jBtrNEXtrsO9Gdg',
      email: 'antoan@qua.bg',
    },
    {
      id: 4,
      firstName: 'Twin',
      lastName: 'Barnes',
      password: '$2b$10$zHKl5PTh3Vx79DCJisnPw.UF0jOWzfMm0jBtrNEXtrsO9Gdg',
      email: 'alyss@abv.bg',
    },
  ];

  const updateToEmployee = {
    firstName: 'Alissandra',
  };

  const mockEmployeesRepository = {
    find: jest.fn().mockImplementation(() => {
      return JSON.parse(JSON.stringify(mockEmployees));
    }),
    findOneOrFail: jest
      .fn()
      .mockImplementation(id => {
        return mockEmployees.find((customer) => customer.id === id);
      }),
    findOne: jest
      .fn()
      .mockImplementation(
        (id) => {
          return mockEmployees.find((customer) => customer.id === id);
        }),
    save: jest.fn().mockImplementation(obj => {
      let customer = mockEmployees.find((c) => c.id === obj.id);
      return customer = Object.assign(customer, obj);
    }),
    delete: jest.fn().mockImplementation((id) => {
      mockEmployees.splice(id - 1, 1);
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EmployeesService,
        {
          provide: getRepositoryToken(Employee),
          useValue: mockEmployeesRepository,
        },
      ],
    }).compile();

    service = module.get<EmployeesService>(EmployeesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return all employees', async () => {
    expect(await service.findAll()).toEqual(mockEmployees);
  });

  it('should return a single employee by id', async () => {
    expect(await service.findOne(1)).toEqual(mockEmployees[0]);
  });

  it('should update the customer', async () => {
    const initialCustomerFirstName = mockEmployees[0].firstName;
    const updatedCustomer = await service.updateEmployee(1, updateToEmployee);
    const newFirstName = updatedCustomer.firstName;
    expect(initialCustomerFirstName).not.toEqual(newFirstName);
  });

  it('should delete the customer', async () => {
    const initialLength = mockEmployees.length;
    await service.deleteEmployee(4);
    expect(mockEmployees.length).toBe(initialLength - 1);
  });
});
