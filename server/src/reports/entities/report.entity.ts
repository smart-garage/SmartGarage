import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  JoinColumn,
  ManyToMany,
  JoinTable,
  BaseEntity,
  RelationId,
  ManyToOne,
  CreateDateColumn,
} from 'typeorm';

import { ObjectType, Field, Int } from '@nestjs/graphql';
import { Service } from 'src/services/entities/service.entity';
import { Customer } from 'src/customers/entities/customer.entity';
import { Vehicle } from 'src/vehicles/entities/vehicle.entity';

@Entity()
@ObjectType('Report')
export class Report extends BaseEntity {
  @Field(() => Int)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @CreateDateColumn()
  date: Date;

  @Field(() => Int)
  @Column({ unique: false })
  vehicleId: number;

  @ManyToOne(() => Vehicle, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  @JoinColumn()
  vehicle: Vehicle;

  @Field(() => Int)
  @Column({ unique: false })
  customerId: number;

  @ManyToOne(() => Customer)
  @JoinColumn()
  customer: Customer;

  @ManyToMany(() => Service, service => service.reports)
  @JoinTable()
  services: Service[];
}
