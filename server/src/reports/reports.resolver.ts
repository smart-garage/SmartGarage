import { Resolver, Query, Mutation, Args, Int, ResolveField, Parent } from '@nestjs/graphql';
import { ReportsService } from './reports.service';
import { Report } from './entities/report.entity';
import { CreateReportInput } from './dto/create-report.input';
import { Service } from 'src/services/entities/service.entity';
import { Vehicle } from 'src/vehicles/entities/vehicle.entity';
import { Customer } from 'src/customers/entities/customer.entity';

@Resolver(() => Report)
export class ReportsResolver {
  constructor(private readonly reportsService: ReportsService) {}

  @ResolveField(() => [Service])
  services(@Parent() report: Report) {
    return report.services;
  }

  @ResolveField(() => Vehicle)
  vehicle(@Parent() report: Report) {
    return report.vehicle;
  }

  @ResolveField(() => Customer)
  customer(@Parent() report: Report) {
    return report.customer;
  }

  @Mutation(() => Report)
  createReport(@Args('createReportInput') createReportInput: CreateReportInput) {
    return this.reportsService.create(createReportInput);
  }

  @Query(() => [Report], { name: 'reports' })
  findAll(@Args('customerId', { type: () => Int }) customerId: number ) {
    return this.reportsService.findAll(customerId);
  }

  @Query(() => Report, { name: 'report' })
  findOne(@Args('id', { type: () => Int }) id: number) {
    return this.reportsService.findOne(id);
  }

  @Mutation(() => Boolean)
  removeReport(@Args('id', { type: () => Int }) id: number) {
    return this.reportsService.remove(id);
  }
}
