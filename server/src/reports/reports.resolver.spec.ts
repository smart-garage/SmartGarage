import { Test, TestingModule } from '@nestjs/testing';
import { ReportsResolver } from './reports.resolver';
import { ReportsService } from './reports.service';

describe('ReportsResolver', () => {
  let resolver: ReportsResolver;

  const mockReportsService = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ReportsResolver, ReportsService],
    })
      .overrideProvider(ReportsService)
      .useValue(mockReportsService)
      .compile();

    resolver = module.get<ReportsResolver>(ReportsResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
