import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ServicesService } from 'src/services/services.service';
import { Repository } from 'typeorm';
import { CreateReportInput } from './dto/create-report.input';
import { Report } from './entities/report.entity';

@Injectable()
export class ReportsService {
  constructor(
    @InjectRepository(Report) private reportsRepository: Repository<Report>,
    private servicesService: ServicesService,
  ) {}

  async create(createReportInput: CreateReportInput): Promise<Report> {
    const report = this.reportsRepository.create(createReportInput);
    const createServices = await this.servicesService.findAll(createReportInput.servicesIds);

    report.services = createServices;
    await this.reportsRepository.save(report);

    return this.reportsRepository.findOneOrFail(report.id, {relations: ['customer', 'vehicle', 'services']});
  }

  async findAll(customerId: number): Promise<Report[]> {
    const report = await this.reportsRepository.find({relations: ['customer', 'vehicle', 'services'], where: { customerId }});

    return report;
  }

  async findOne(id: number): Promise<Report> {
    return await this.reportsRepository.findOneOrFail(id, {relations: ['customer', 'vehicle', 'services']});
  }

  async remove(id: number): Promise<boolean> {
    await this.reportsRepository.findOneOrFail(id);

    await this.reportsRepository.delete(id);

    return true;
  }
}
