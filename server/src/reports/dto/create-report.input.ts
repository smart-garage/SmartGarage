import { InputType, Int, Field } from '@nestjs/graphql';

@InputType()
export class CreateReportInput {
  @Field(() => Int)  
  vehicleId: number;
 
  @Field(() => Int)
  customerId: number;

  @Field(() => [Int])
  servicesIds: number[];
}