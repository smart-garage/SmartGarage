import { InputType, Field, Int } from '@nestjs/graphql';
import { IsNotEmpty, IsNumberString, Length, Matches } from 'class-validator';

@InputType()
export class CreateVehicleInput {
  @IsNotEmpty({ message: "Please provide the vehicle's manufacturer" })
  @Field()
  manufacturer: string;

  @IsNotEmpty({ message: "Please provide the vehicle's model" })
  @Field()
  model: string;

  @Length(4, 4, { message: 'Please enter a valid year!' })
  @IsNumberString({}, { message: 'Please enter a valid year!' })
  @Field()
  year: string;

  @Length(17, 17, {
    message: 'The vin must be exactly $constraint1 characters long!',
  })
  @Field()
  vin: string;

  @Matches(/^[A-Z]{1,2}\d{4}[A-Z]{2}/, { message: 'Invalid license plate!' })
  @Field()
  registration_plate: string;

  @Field()
  email: string;

  customerId?: number;
}
