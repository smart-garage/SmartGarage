import {
  Resolver,
  Query,
  Mutation,
  Args,
  Int,
  ResolveField,
  Parent,
} from '@nestjs/graphql';
import { VehiclesService } from './vehicles.service';
import { Vehicle } from './entities/vehicle.entity';
import { CreateVehicleInput } from './dto/create-vehicle.input';
import { UpdateVehicleInput } from './dto/update-vehicle.input';
import { Customer } from 'src/customers/entities/customer.entity';
import { UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { RolesGuard } from 'src/auth/roles.guard';
import { Roles } from 'src/auth/roles.decorator';

@Resolver(() => Vehicle)
export class VehiclesResolver {
  constructor(private readonly vehiclesService: VehiclesService) {}

  @Query(() => [Vehicle])
  @Roles('employee')
  @UseGuards(JwtAuthGuard, RolesGuard)
  vehicles(): Promise<Vehicle[]> {
    return this.vehiclesService.findAll();
  }

  @Query(() => Vehicle)
  vehicle(@Args('id', { type: () => Int }) id: number): Promise<Vehicle> {
    return this.vehiclesService.findOne(id);
  }

  @Query(() => Vehicle)
  vehicleByLicensePlate(@Args('vehicle') vehicle: string): Promise<Vehicle> {
    return this.vehiclesService.findByLicensePlate(vehicle);
  }

  @ResolveField(() => Customer)
  @Roles('employee')
  @UseGuards(JwtAuthGuard, RolesGuard)
  customer(@Parent() vehicle: Vehicle): Promise<Customer> {
    return this.vehiclesService.getCustomer(vehicle.customerId);
  }

  @Mutation(() => Vehicle)
  @Roles('employee')
  @UseGuards(JwtAuthGuard, RolesGuard)
  createVehicle(
    @Args('createVehicleInput') createVehicleInput: CreateVehicleInput,
  ): Promise<Vehicle> {
    return this.vehiclesService.create(createVehicleInput);
  }

  @Mutation(() => Vehicle)
  @Roles('employee')
  @UseGuards(JwtAuthGuard, RolesGuard)
  updateVehicle(
    @Args('id', { type: () => Int }) id: number,
    @Args('updateVehicleInput') updateVehicleInput: UpdateVehicleInput,
  ): Promise<Vehicle> {
    return this.vehiclesService.update(id, updateVehicleInput);
  }

  @Mutation(() => String)
  @Roles('employee')
  @UseGuards(JwtAuthGuard, RolesGuard)
  removeVehicle(@Args('id', { type: () => Int }) id: number): Promise<string> {
    return this.vehiclesService.remove(id);
  }
}
