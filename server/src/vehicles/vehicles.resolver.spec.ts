import { Test, TestingModule } from '@nestjs/testing';
import { VehiclesResolver } from './vehicles.resolver';
import { VehiclesService } from './vehicles.service';

describe('VehiclesResolver', () => {
  let resolver: VehiclesResolver;

  const mockCreateVehicle = {
    manufacturer: 'skoda',
    model: 'octavia',
    year: '2019',
    vin: '12345678912345678',
    registration_plate: 'PA9345LB',
    customerId: 1,
    email: 'tim@bigcorp.com'
  };

  const mockUpdateVehicle = {
    manufacturer: 'skoda',
    model: 'octavia',
    year: '2019',
    vin: '12345678912345678',
    registration_plate: 'PA9345LB',
    customerId: 1,
  };

  const mockVehicles = [
    {
      id: 1,
      manufacturer: 'bmw',
      model: 'i8',
      year: '2020',
      vin: '12345678912345678',
      registration_plate: 'EN6677VN',
      customerId: 1,
    },
    {
      id: 2,
      manufacturer: 'audi',
      model: 'a6',
      year: '2015',
      vin: '67438960321754398',
      registration_plate: 'A8953BV',
      customerId: 1,
    },
    {
      id: 3,
      manufacturer: 'mazda',
      model: '3',
      year: '2018',
      vin: '93750128543920348',
      registration_plate: 'SA2927OT',
      customerId: 1,
    },
  ];

  const mockVehicleService = {
    create: jest.fn((dto) => {
      return { id: Date.now(), ...dto };
    }),
    update: jest.fn((id, dto) => {
      return {
        id,
        ...dto,
      };
    }),
    findAll: jest.fn(() => mockVehicles),
    findOne: jest.fn(
      (id) => mockVehicles.filter((vehicle) => vehicle.id === id)[0],
    ),
    remove: jest.fn((id) => {
      mockVehicles.splice(id - 1);
      return 'Successfully deleted this vehicle!';
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [VehiclesResolver, VehiclesService],
    })
      .overrideProvider(VehiclesService)
      .useValue(mockVehicleService)
      .compile();

    resolver = module.get<VehiclesResolver>(VehiclesResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });

  it('should create a vehicle', async () => {
    expect(resolver.createVehicle(mockCreateVehicle)).toEqual({
      id: expect.any(Number),
      ...mockCreateVehicle,
    });

    expect(mockVehicleService.create).toHaveBeenCalledWith(mockCreateVehicle);
  });

  it('should update a vehicle', async () => {
    expect(resolver.updateVehicle(1, mockUpdateVehicle)).toEqual({
      id: 1,
      ...mockUpdateVehicle,
    });

    expect(mockVehicleService.update).toHaveBeenCalledWith(
      1,
      mockUpdateVehicle,
    );
  });

  it('should return all vehicles', async () => {
    expect(resolver.vehicles()).toEqual(mockVehicles);
  });

  it('should return a single vehicle', async () => {
    expect(resolver.vehicle(1)).toEqual(mockVehicles[0]);
  });

  it('should remove a vehicle', async () => {
    expect(resolver.removeVehicle(1)).toEqual(
      'Successfully deleted this vehicle!',
    );

    expect(resolver.vehicle(1)).toEqual(undefined);
  });
});
