import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  BaseEntity,
} from 'typeorm';
import { ObjectType, Field, Int } from '@nestjs/graphql';
import { Customer } from 'src/customers/entities/customer.entity';

@Entity()
@ObjectType('Vehicle')
export class Vehicle extends BaseEntity {
  @Field(() => Int)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  manufacturer: string;

  @Field()
  @Column()
  model: string;

  @Field()
  @Column()
  year: string;

  @Field()
  @Column()
  vin: string;

  @Field()
  @Column()
  registration_plate: string;

  @Field(() => Int)
  @Column()
  customerId: number;

  @ManyToOne(() => Customer, (customer) => customer.vehicles)
  customer: Customer;
}
