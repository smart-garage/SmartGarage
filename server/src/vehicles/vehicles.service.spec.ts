import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { CustomersService } from 'src/customers/customers.service';
import { Customer } from 'src/customers/entities/customer.entity';
import { Vehicle } from './entities/vehicle.entity';
import { VehiclesService } from './vehicles.service';

describe('VehiclesService', () => {
  let service: VehiclesService;

  const vehicleUpdate = {
    id: 1,
    model: 'enyaq iv',
    year: '2021',
  };

  // const vehicleToCreate = {
  //   manufacturer: 'skoda',
  //   model: 'octavia',
  //   year: '2019',
  //   vin: '12345678912345678',
  //   registration_plate: 'PA9345LB',
  //   email: 'mock2@mock.com'
  // };

  const mockVehicles = [
    {
      id: 1,
      manufacturer: 'bmw',
      model: 'i8',
      year: '2020',
      vin: '12345678912345678',
      registration_plate: 'EN6677VN',
    },
    {
      id: 2,
      manufacturer: 'audi',
      model: 'a6',
      year: '2015',
      vin: '67438960321754398',
      registration_plate: 'A8953BV',
    },
    {
      id: 3,
      manufacturer: 'mazda',
      model: '3',
      year: '2018',
      vin: '93750128543920348',
      registration_plate: 'SA2927OT',
    },
  ];

  const mockUserRepo = {
    findOneOrFail: jest
      .fn()
      .mockImplementation(
        (id) => mockCustomers.filter((customer) => customer.id === id)[0],
      ),
    findOneByMail: jest
    .fn()
    .mockImplementation(
      (email) => mockCustomers.find((customer) => customer.email === email),
    ),
  };

  const mockCustomers = [
    {
      id: 1,
      email: 'mock@mock.com',
    },
    {
      id: 2,
      email: 'mock2@mock.com',
    },
  ];

  const mockVehiclesRepository = {
    find: jest.fn().mockImplementation(() => mockVehicles),
    findOneOrFail: jest
      .fn()
      .mockImplementation(
        (id) => mockVehicles.filter((vehicle) => vehicle.id === id)[0],
      ),
    create: jest.fn().mockImplementation((dto) => dto),
    save: jest
      .fn()
      .mockImplementation((vehicle) =>
        Promise.resolve({ id: Date.now(), ...vehicle }),
      ),
    delete: jest.fn().mockImplementation((id) => {
      mockVehicles.splice(id - 1, 1);
      return 'Successfully deleted this vehicle!';
    }),
    getCustomer: jest
      .fn()
      .mockImplementation(
        (id) => mockCustomers.filter((customer) => customer.id === id)[0],
      ),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        VehiclesService,
        {
          provide: getRepositoryToken(Vehicle),
          useValue: mockVehiclesRepository,
        },
        CustomersService,
        {
          provide: getRepositoryToken(Customer),
          useValue: mockUserRepo,
        },
      ],
    }).compile();

    service = module.get<VehiclesService>(VehiclesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  // it('should create and save a new vehicle', async () => {
  //   expect(await service.create(vehicleToCreate)).toEqual({
  //     ...vehicleToCreate,
  //     id: expect.any(Number),
  //   });
  // });

  it('should return all vehicles', async () => {
    expect(await service.findAll()).toEqual(mockVehicles);
  });

  it('should return a single vehicle', async () => {
    expect(await service.findOne(1)).toEqual(mockVehicles[0]);
  });

  it('should update a vehicle', async () => {
    expect(await service.update(1, vehicleUpdate)).toEqual(mockVehicles[0]);
  });

  it('should remove a vehicle', async () => {
    expect(await service.remove(1)).toEqual(
      'Sucessfully deleted this vehicle!',
    );
  });

  it('should get a customer by their id found in the vehicle entity', async () => {
    expect(await service.getCustomer(1)).toEqual(mockCustomers[0]);
  });
});
