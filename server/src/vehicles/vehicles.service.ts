import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CustomersService } from 'src/customers/customers.service';
import { Customer } from 'src/customers/entities/customer.entity';
import { Repository } from 'typeorm';
import { CreateVehicleInput } from './dto/create-vehicle.input';
import { UpdateVehicleInput } from './dto/update-vehicle.input';
import { Vehicle } from './entities/vehicle.entity';

@Injectable()
export class VehiclesService {
  constructor(
    @InjectRepository(Vehicle) private vehiclesRepository: Repository<Vehicle>,
    @Inject(forwardRef(() => CustomersService))
    private customersService: CustomersService,
  ) {}

  async create(createVehicleInput: CreateVehicleInput): Promise<Vehicle> {
    const customer = await this.customersService.findOneByMail(createVehicleInput.email);
    createVehicleInput.customerId = customer.id;
    const newVehicle = this.vehiclesRepository.create(createVehicleInput);

    return await this.vehiclesRepository.save(newVehicle);
  }

  async findAll(): Promise<Vehicle[]> {
    return await this.vehiclesRepository.find();
  }

  async findOne(id: number): Promise<Vehicle> {
    return await this.vehiclesRepository.findOneOrFail(id);
  }

  async update(
    id: number,
    updateVehicleInput: UpdateVehicleInput,
  ): Promise<Vehicle> {
    await this.vehiclesRepository.findOneOrFail(id);

    await this.vehiclesRepository.save({
      id,
      ...updateVehicleInput,
    });

    return await this.vehiclesRepository.findOneOrFail(id);
  }

  async remove(id: number): Promise<string> {
    await this.vehiclesRepository.findOneOrFail(id);

    await this.vehiclesRepository.delete(id);

    return 'Sucessfully deleted this vehicle!';
  }

  async getCustomer(id: number): Promise<Customer> {
    return this.customersService.findOne(id);
  }

  async findByLicensePlate(registration_plate: string): Promise<Vehicle> {
    return await this.vehiclesRepository.findOne({ where: { registration_plate }})
  }
}
