import { Injectable, Dependencies } from '@nestjs/common';
import { GqlExecutionContext } from "@nestjs/graphql";
import { Reflector } from '@nestjs/core';

// might not need it, but keeping it in case
@Injectable()
@Dependencies(Reflector)
export class UserIdAndRolesGuard{
  reflector;
    constructor(reflector) {
    this.reflector = reflector;
  }

  canActivate(context) {
    const roles = this.reflector.get('roles', context.getHandler());
    const userId = this.reflector.get('userId', context.getHandler());
    const ctx = GqlExecutionContext.create(context);
    const user = ctx.getContext().req.user;

    if (!userId && !roles) {
      return true;
    }

    if ((roles.includes(user.role)) || userId === user.id){
      return true;
    }

    return false;
  }
}
