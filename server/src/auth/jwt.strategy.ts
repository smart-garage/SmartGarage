import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";
import * as dotenv from 'dotenv';

const config = dotenv.config().parsed;

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, "jwt") {
    constructor() {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: config.PRIVATE_KEY,
        })
    }

    async validate(payload: any) {
        return { // this becomes available in the req.user
            email: payload.email,
            id: payload.sub,
            role: payload.role
        }
    }
}
