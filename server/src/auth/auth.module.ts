import { forwardRef, Module } from '@nestjs/common';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { CustomersModule } from 'src/customers/customers.module';
import { EmployeesModule } from 'src/employees/employees.module';
import { AuthService } from './auth.service';
import * as dotenv from 'dotenv';
import { JwtStrategy } from './jwt.strategy';
import { JwtAuthGuard } from './jwt-auth.guard';
import { RolesGuard } from './roles.guard';

const config = dotenv.config().parsed;

@Module({
  imports: [
    forwardRef(() => CustomersModule),
    forwardRef(() => EmployeesModule),
    PassportModule.register({defaultStrategy: "jwt"}),
    JwtModule.register({
      secret: config.PRIVATE_KEY,
      signOptions: { 
        expiresIn: config.TOKEN_LIFETIME 
      },
    })
  ],
  providers: [AuthService, JwtStrategy, JwtAuthGuard, RolesGuard],
  exports: [AuthService],
})
export class AuthModule { }
