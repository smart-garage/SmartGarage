import { InputType, Field } from '@nestjs/graphql';
import { MinLength } from 'class-validator';

@InputType()
export class ChangeForgottenPasswordInput {
  @Field()
  token: string;

  @Field()
  @MinLength(8, { message: 'Password must have at least 8 characters!' })
  password: string;
}
