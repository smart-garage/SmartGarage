import { Injectable, Dependencies } from '@nestjs/common';
import { GqlExecutionContext } from "@nestjs/graphql";
import { Reflector } from '@nestjs/core';

@Injectable()
@Dependencies(Reflector)
export class RolesGuard {
  reflector;
    constructor(reflector) {
    this.reflector = reflector;
  }

  canActivate(context) {
    const roles = this.reflector.get('roles', context.getHandler());
    if (!roles) {
      return true;
    }
    const ctx = GqlExecutionContext.create(context);
    const user = ctx.getContext().req.user;

    return (roles.includes(user.role)); // check if role matches
  }
}
