import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { Customer } from 'src/customers/entities/customer.entity';
import { Employee } from 'src/employees/entities/employee.entity';

@Injectable()
export class AuthService {
constructor(private jwtService: JwtService) {}

    async validatePassword(user: Customer | Employee, pass: string): Promise<any> {
        const validPass = await bcrypt.compare(pass, user.password);
        console.log(pass, user.password, validPass)

        if (validPass) {
            const { password, ...result } = user;
            return result;
        }

        return null;
    }

    async CreateAccessToken(user: Customer | Employee) { // !user problem 
        const payload = {
            email: user.email,
            sub: user.id,
            role: user.role
        };
        // whitelist token in Redis?
        return {
           access_token:
           this.jwtService.sign(payload)
        };
    }
}
