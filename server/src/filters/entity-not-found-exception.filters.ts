import { Catch, NotFoundException } from '@nestjs/common';
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError';
import { GqlExceptionFilter } from '@nestjs/graphql';

/**
 * Custom exception filter to convert EntityNotFoundError from TypeOrm to NestJs responses
 * @see also @https://docs.nestjs.com/exception-filters
 */
@Catch(EntityNotFoundError)
export class EntityNotFoundExceptionFilter implements GqlExceptionFilter {
  public catch(exception: EntityNotFoundError) {
    const entity = exception.message.split(' ')[7].replace(/['"]+/g, '');

    throw new NotFoundException(`${entity} not found!`);
  }
}
