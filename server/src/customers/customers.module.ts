import { forwardRef, Module } from '@nestjs/common';
import { CustomersService } from './customers.service';
import { CustomersResolver } from './customers.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Customer } from './entities/customer.entity';
import { VehiclesModule } from 'src/vehicles/vehicles.module';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  imports: [
    forwardRef(() => AuthModule),
    TypeOrmModule.forFeature([Customer]),
    forwardRef(() => VehiclesModule),
  ],
  providers: [CustomersResolver, CustomersService ],
  exports: [CustomersService],
})
export class CustomersModule {}
