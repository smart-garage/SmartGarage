import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
  BaseEntity,
} from 'typeorm';
import { ObjectType, Field, Int } from '@nestjs/graphql';
import { Vehicle } from 'src/vehicles/entities/vehicle.entity';
import { Report } from 'src/reports/entities/report.entity';

@Entity()
@ObjectType('Customer')
export class Customer extends BaseEntity {
  @Field(() => Int)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column({ default: "regularUser" }) // possibly take out in enum? or check if instanceof
  role: string;

  // the e-mail is the username
  @Field()
  @Column({ unique: true })
  email: string;

  @Field()
  @Column()
  firstName: string;

  @Field()
  @Column()
  lastName: string;

  @Field()
  @Column()
  password: string;

  // has the user confirmed their account to allow login
  @Column('bool', { default: false })
  confirmed: boolean;
  // then in login - if (!user.confirmed) return null; - so that they can't log in

  @Field()
  @Column()
  phone: string;

  @OneToMany(() => Vehicle, (vehicle) => vehicle.customer, { onDelete: "CASCADE" })
  vehicles: Vehicle[]; // ids?

  @OneToMany(() => Report, (report) => report.customer)
  reports: Report[];
}

// First & Last names – no less than 2 characters, no more than 20
// Phone – a 10-digit string in the format 0xxxxxxxxx
// Email – unique in the system
// Password – at least 8 characters
