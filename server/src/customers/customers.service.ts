import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCustomerInput } from './dto/create-customer.input';
import { UpdateCustomerInput } from './dto/update-customer.input';
import { Customer } from './entities/customer.entity';
import * as bcrypt from 'bcrypt';
import { generatePassword } from 'src/auth/password-generator';
import { sendRegistrationEmail } from 'src/utils/send-registration-email';
import { createConfirmationUrl } from 'src/utils/create-confirmation-url';
import { VehiclesService } from 'src/vehicles/vehicles.service';

const saltRounds = 10;

@Injectable()
export class CustomersService {
  constructor(
    @InjectRepository(Customer)
    private customersRepository: Repository<Customer>,
    // do we need this? not using it
    @Inject(forwardRef(() => VehiclesService))
    private vehiclesService: VehiclesService,
  ) { }

  // Get all customers
  async findAll(): Promise<Customer[]> {
    return await this.customersRepository.find();
  }

  // Get one customer by id
  async findOne(id: number): Promise<Customer> {
    return await this.customersRepository.findOneOrFail(id);
  }

  // Get one customer by e-mail
  async findOneByMail(email: string): Promise<Customer> {
    return await this.customersRepository.findOneOrFail({ where: { email } });
  }

  // Create a customer
  async createCustomer(
    createCustomerInput: CreateCustomerInput,
  ): Promise<Customer> {
    const { email } = createCustomerInput;
    const password = generatePassword();
    const existingCheck = await this.customersRepository.findOne({ where: { email } });
    if (existingCheck) {
      throw new Error('E-mail already in the system!');
    }
    const hashedPassword = await bcrypt.hash(password, saltRounds);

    const newCustomer = this.customersRepository.create({
      password: hashedPassword,
      ...createCustomerInput,
    });
    // send pass and confirmation URL per mail
    await sendRegistrationEmail(
      email,
      password,
      await createConfirmationUrl(+newCustomer.id),
    );

    return await this.customersRepository.save({
      ...newCustomer,
      hashedPassword,
    });
  }

  // Update a customer by id
  async updateCustomer(id: number, updateCustomerInput: UpdateCustomerInput): Promise<Customer> {
    await this.customersRepository.findOneOrFail(id);
    await this.customersRepository.save({ id, ...updateCustomerInput });

    return await this.customersRepository.findOne(id);
  }

  // Delete customer by id
  async deleteCustomer(id: number): Promise<Customer> {
    const deletedCustomer = await this.customersRepository.findOneOrFail(id);

    await this.customersRepository.delete(id);

    return deletedCustomer;
  }

  // Change password
  async changePassword(id: number, password: string) {
    const hashedPassword = await bcrypt.hash(password, saltRounds);
    await this.customersRepository.update(id, {
      password: hashedPassword,
    });
  }
}
