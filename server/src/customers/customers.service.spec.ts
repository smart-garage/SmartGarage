import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Vehicle } from 'src/vehicles/entities/vehicle.entity';
import { VehiclesService } from 'src/vehicles/vehicles.service';
import { CustomersService } from './customers.service';
import { Customer } from './entities/customer.entity';

describe('CustomersService', () => {
  let service: CustomersService;

  const mockCustomers: any[] = [
    {
      id: 1,
      firstName: 'Alissa',
      lastName: 'Rhymes',
      password: '$2b$10$zHKl5PTh3Vx79DCJisnPw.UF0jOWzfMm0jBtrNEXtrsO9Gdg',
      email: 'alyss@abv.bg',
      phone: '0888266758',
      confirmed: false,
    },
    {
      id: 2,
      firstName: 'Ivelin',
      lastName: 'QL',
      password: '$2b$10$zHKl5PTh3Vx79DCJisnPw.UF0jOWzfMm0jBtrNEXtrsO9Gdg',
      email: 'ivelincho@dollars.com',
      phone: '0899266758',
      confirmed: true,
    },
    {
      id: 3,
      firstName: 'Toni',
      lastName: 'Bonboni',
      password: '$2b$10$zHKl5PTh3Vx79DCJisnPw.UF0jOWzfMm0jBtrNEXtrsO9Gdg',
      email: 'antoan@qua.bg',
      phone: '0887266758',
      confirmed: false,
    },
    {
      id: 4,
      firstName: 'Twin',
      lastName: 'Barnes',
      password: '$2b$10$zHKl5PTh3Vx79DCJisnPw.UF0jOWzfMm0jBtrNEXtrsO9Gdg',
      email: 'alyss@abv.bg',
      phone: '0898546758',
      confirmed: false,
    },
  ];

  const updateToCustomer = {
    firstName: 'Alissandra',
  };

  const mockCustomersRepository = {
    find: jest.fn().mockImplementation(() => {
      return JSON.parse(JSON.stringify(mockCustomers));
    }),
    findOneOrFail: jest
      .fn()
      .mockImplementation(id => {
        return mockCustomers.find((customer) => customer.id === id);
      }),
    findOne: jest
      .fn()
      .mockImplementation(
        (id) => {
          return mockCustomers.find((customer) => customer.id === id);
        }),
    save: jest.fn().mockImplementation(obj => {
      let customer = mockCustomers.find((c) => c.id === obj.id);
      return customer = Object.assign(customer, obj);
    }),
    delete: jest.fn().mockImplementation((id) => {
      mockCustomers.splice(id - 1, 1);
    }),
  };

  const mockVehicles = [
    {
      id: 1,
      manufacturer: 'bmw',
      model: 'i8',
      year: '2020',
      vin: '12345678912345678',
      registration_plate: 'EN6677VN',
    },
    {
      id: 2,
      manufacturer: 'audi',
      model: 'a6',
      year: '2015',
      vin: '67438960321754398',
      registration_plate: 'A8953BV',
    },
  ];

  const mockVehiclesRepository = {
    find: jest.fn().mockImplementation(() => mockVehicles),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CustomersService,
        {
          provide: getRepositoryToken(Customer),
          useValue: mockCustomersRepository,
        },
        VehiclesService,
        {
          provide: getRepositoryToken(Vehicle),
          useValue: mockVehiclesRepository,
        },
      ],
    }).compile();

    service = module.get<CustomersService>(CustomersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return all customers', async () => {
    expect(await service.findAll()).toEqual(mockCustomers);
  });

  it('should return a single customer by id', async () => {
    expect(await service.findOne(1)).toEqual(mockCustomers[0]);
  });

  it('should update the customer', async () => {
    const initialCustomerFirstName = mockCustomers[0].firstName;
    const updatedCustomer = await service.updateCustomer(1, updateToCustomer);
    const newFirstName = updatedCustomer.firstName;
    expect(initialCustomerFirstName).not.toEqual(newFirstName);
  });

  it('should delete the customer', async () => {
    const initialLength = mockCustomers.length;
    await service.deleteCustomer(4);
    expect(mockCustomers.length).toBe(initialLength - 1);
  });
});
