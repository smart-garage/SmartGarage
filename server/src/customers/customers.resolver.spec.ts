import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from 'src/auth/auth.service';
import { CustomersResolver } from './customers.resolver';
import { CustomersService } from './customers.service';

describe('CustomersResolver', () => {
  let resolver: CustomersResolver;

  const mockCustomers = [
    {
      id: 1,
      firstName: 'Alissa',
      lastName: 'Rhymes',
      password: '$2b$10$zHKl5PTh3Vx79DCJisnPw.UF0jOWzfMm0jBtrNEXtrsO9Gdg',
      email: 'alyss@abv.bg',
      phone: '0888266758',
      confirmed: false,
    },
    {
      id: 2,
      firstName: 'Ivelin',
      lastName: 'QL',
      password: '$2b$10$zHKl5PTh3Vx79DCJisnPw.UF0jOWzfMm0jBtrNEXtrsO9Gdg',
      email: 'ivelincho@dollars.com',
      phone: '0899266758',
      confirmed: true,
    },
    {
      id: 3,
      firstName: 'Toni',
      lastName: 'Bonboni',
      password: '$2b$10$zHKl5PTh3Vx79DCJisnPw.UF0jOWzfMm0jBtrNEXtrsO9Gdg',
      email: 'antoan@qua.bg',
      phone: '0887266758',
      confirmed: false,
    },
    {
      id: 4,
      firstName: 'Twin',
      lastName: 'Barnes',
      password: '$2b$10$zHKl5PTh3Vx79DCJisnPw.UF0jOWzfMm0jBtrNEXtrsO9Gdg',
      email: 'alyss@abv.bg',
      phone: '0898546758',
      confirmed: false,
    },
  ];

  const updateToCustomer = {
    firstName: 'Alissandra',
  };

  const mockCustomerService = {
    findAll: jest.fn(() => mockCustomers),
    findOne: jest.fn(
      (id) => mockCustomers.filter((vehicle) => vehicle.id === id)[0],
    ),
    updateCustomer: jest.fn((id, dto) => {
      return {
        id,
        ...dto,
      };
    }),
    deleteCustomer: jest.fn((id) => {
      mockCustomers.splice(id - 1, 1);
    }),
  };

  const mockAuthService = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CustomersResolver, CustomersService, AuthService],
    })
      .overrideProvider(CustomersService)
      .useValue(mockCustomerService)
      .overrideProvider(AuthService)
      .useValue(mockAuthService)
      .compile();

    resolver = module.get<CustomersResolver>(CustomersResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });

  it('should return all customers', async () => {
    expect(resolver.customers()).toEqual(mockCustomers);
  });

  it('should return a single customer', async () => {
    expect(resolver.customerById(1)).toEqual(mockCustomers[0]);
  });

  it('should update a customer', async () => {
    const initialCustomerFirstName = mockCustomers[0].firstName;
    const newFirstName = (await resolver.updateCustomer(1, updateToCustomer))
      .firstName;
    expect(initialCustomerFirstName).not.toEqual(newFirstName);
  });

  it('should delete a customer', async () => {
    const initialLength = mockCustomers.length;
    await resolver.removeCustomer(initialLength - 1);
    expect(mockCustomers.length).toEqual(initialLength - 1);
  });
});

function AuthenticationService(AuthenticationService: any) {
  throw new Error('Function not implemented.');
}

