import { Resolver, Query, Mutation, Args, Int, GqlExecutionContext } from '@nestjs/graphql';
import { CustomersService } from './customers.service';
import { Customer } from './entities/customer.entity';
import { CreateCustomerInput } from './dto/create-customer.input';
import { UpdateCustomerInput } from './dto/update-customer.input';
import { redis } from 'src/redis';
import { createPassResetUrl } from 'src/utils/create-pass-reset-url';
import { sendPassResetEmail } from 'src/utils/send-password-reset-email';
import { ChangeForgottenPasswordInput } from '../auth/dto/change-forgotten-password.input';
import { LoginInput } from '../auth/dto/login.input';
import { AuthService } from 'src/auth/auth.service';
import { UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { RolesGuard } from 'src/auth/roles.guard';
import { Roles } from 'src/auth/roles.decorator';
// import { UserIdAndRolesGuard } from 'src/auth/userId-and-roles.guard';

@Resolver(() => Customer)
export class CustomersResolver {
  constructor(private readonly customersService: CustomersService,
    private readonly authService: AuthService) { }

  // Get all customers
  @Query(() => [Customer])
  @Roles('employee')
  @UseGuards(JwtAuthGuard, RolesGuard)
  customers(): Promise<Customer[]> {
    return this.customersService.findAll();
  }

  // Get one customer by id - admin
  @Query(() => Customer)
  @Roles('employee')
  @UseGuards(JwtAuthGuard, RolesGuard)
  customerById(@Args('id', { type: () => Int }) id: number) {
    return this.customersService.findOne(id);
  }

  // Get own profile
  @Query(() => Customer)
  @UseGuards(JwtAuthGuard)
  getSelfCustomerById(
    @Args('loggedCustomerId', { type: () => Int }) loggedCustomerId: number,
    @Args('id', { type: () => Int }) id: number) {
    if (loggedCustomerId === id) {
      return this.customersService.findOne(id);
    }
    return `Unauthorized!`;
  }

  // Get one customer by e-mail
  @Query(() => Customer)
  @Roles('employee')
  @UseGuards(JwtAuthGuard, RolesGuard)
  customerByMail(@Args('email', { type: () => String }) email: string) {
    return this.customersService.findOneByMail(email);
  }

  // Create customer - register by employee
  @Mutation(() => Customer)
  @Roles('employee')
  @UseGuards(JwtAuthGuard, RolesGuard)
  createCustomer(
    @Args('createCustomerInput') createCustomerInput: CreateCustomerInput,
  ) {
    return this.customersService.createCustomer(createCustomerInput);
  }

  // Update customer by id
  @Mutation(() => Customer)
  @Roles('employee')
  @UseGuards(JwtAuthGuard, RolesGuard)
  updateCustomer(
    @Args('id', { type: () => Int }) id: number,
    @Args('updateCustomerInput') updateCustomerInput: UpdateCustomerInput,
  ) {
    return this.customersService.updateCustomer(id, updateCustomerInput);
  }

  // Delete customer by id
  @Mutation(() => Customer)
  @Roles('employee')
  @UseGuards(JwtAuthGuard, RolesGuard)
  removeCustomer(@Args('id', { type: () => Int }) id: number) {
    return this.customersService.deleteCustomer(id);
  }

  // Confirm customer - enable login
  @Mutation(() => String)
  async confirmCustomer(@Args('token') token: string) {
    const customerId = await redis.get(token);

    if (!customerId) {
      return `Account cannot be confirmed!`;
    }

    this.customersService.updateCustomer(+customerId, {
      ...UpdateCustomerInput,
      confirmed: true,
    });

    return 'Account confirmed!';
  }

  // Customer login
  @Mutation(() => String)
  async loginCustomer(@Args('loginInput') loginInput: LoginInput) {
    const { email, password } = loginInput;

    const user = await this.customersService.findOneByMail(email); // check if this doesn't throw..
    if (!user) {
      return 'Invalid credentials!' // or throw new Error()?
    }

    const validPass = await this.authService.validatePassword(user, password);
    if (!validPass) {
      return 'Invalid credentials!' // or throw new Error()?
    }

    const access_token = (await this.authService.CreateAccessToken(user)).access_token;
    return access_token;
  }

  // Forgotten password
  @Mutation(() => String)
  async forgotPasswordCustomer(@Args('email') email: string): Promise<string> {
    const customer = await this.customersService.findOneByMail(email);
    await sendPassResetEmail(email, await createPassResetUrl(customer.id));

    return 'Password reset e-mail sent!';
  }

  // Change password
  @UseGuards(JwtAuthGuard)
  @Mutation(() => String)
  async changePasswordCustomer(
    @Args('id', { type: () => Int }) id: number,
    @Args('password', { type: () => String }) password: string,
  ) {

    await this.customersService.changePassword(id, password);
    // expire token?
    return 'Password changed!';
  }

  // Change forgotten password
  @Mutation(() => String)
  async changeForgottenPasswordCustomer(
    @Args('changeForgottenPasswordInput') { token, password }: ChangeForgottenPasswordInput,
  ) {
    const customerId = await redis.get(token);

    if (!customerId) {
      return 'Something went wrong!';
    }
    await this.customersService.changePassword(+customerId, password);
    // expire token?
    return 'Password changed!';
  }
}
