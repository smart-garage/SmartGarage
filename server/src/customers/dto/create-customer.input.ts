import { InputType, Int, Field } from '@nestjs/graphql';
import { IsEmail, Length, Matches } from 'class-validator';
import { Vehicle } from 'src/vehicles/entities/vehicle.entity';

@InputType()
export class CreateCustomerInput {
  @Field()
  @IsEmail()
  @Length(1, 60, { message: 'E-mail must be between 2 and 60 characters!' })
  email: string;

  @Field()
  @Length(2, 30, { message: 'Name must be between 2 and 30 characters!' })
  firstName: string;

  @Field()
  @Length(2, 30, { message: 'Name must be between 2 and 30 characters!' })
  lastName: string;

  @Field()
  @Matches(/0[0-9]{9}$/, {
    message:
      'Phone number must a 10-digit number string in the format 0xxxxxxxxx!',
  })
  phone: string;

  // eslint-disable-next-line @typescript-eslint/no-inferrable-types
  @Field()
  confirmed: boolean = false;
}
