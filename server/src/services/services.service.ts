import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateServiceInput } from './dto/create-service.input';
import { UpdateServiceInput } from './dto/update-service.input';
import { Service } from './entities/service.entity';

@Injectable()
export class ServicesService {
  constructor(
    @InjectRepository(Service) private servicesRepository: Repository<Service>,
  ) {}

  async create(createServiceInput: CreateServiceInput): Promise<Service> {
    const newService = this.servicesRepository.create(createServiceInput);

    return this.servicesRepository.save(newService);
  }

  async findAll(ids?: number[]): Promise<Service[]> {
    return ids ? this.servicesRepository.findByIds(ids) : this.servicesRepository.find();
  }

  async findOne(id: number): Promise<Service> {
    return this.servicesRepository.findOneOrFail(id);
  }

  async update(
    id: number,
    updateServiceInput: UpdateServiceInput,
  ): Promise<Service> {
    await this.servicesRepository.findOneOrFail(id);

    await this.servicesRepository.save({
      id,
      ...updateServiceInput,
    });

    return this.servicesRepository.findOneOrFail(id);
  }

  async remove(ids: number[]): Promise<string> {
    ids.length === 1 ? await this.servicesRepository.findOneOrFail(ids[0]) : await this.servicesRepository.findByIds(ids);

    await this.servicesRepository.delete(ids);

    return 'Successfully deleted this service!';
  }
}
