import { Resolver, Query, Mutation, Args, Int } from '@nestjs/graphql';
import { ServicesService } from './services.service';
import { Service } from './entities/service.entity';
import { CreateServiceInput } from './dto/create-service.input';
import { UpdateServiceInput } from './dto/update-service.input';
import { UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { RolesGuard } from 'src/auth/roles.guard';
import { Roles } from 'src/auth/roles.decorator';

@Resolver(() => Service)
export class ServicesResolver {
  constructor(private readonly servicesService: ServicesService) {}

  @Mutation(() => Service)
  @Roles('employee')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async createService(
    @Args('createServiceInput') createServiceInput: CreateServiceInput,
  ): Promise<Service> {
    return await this.servicesService.create(createServiceInput);
  }

  @Query(() => [Service], { name: 'services' })
  @Roles('employee')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async findAll(): Promise<Service[]> {
    return await this.servicesService.findAll();
  }

  @Query(() => Service, { name: 'service' })
  async findOne(@Args('id', { type: () => Int }) id: number): Promise<Service> {
    return await this.servicesService.findOne(id);
  }

  @Mutation(() => Service)
  @Roles('employee')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async updateService(
    @Args('id', { type: () => Int }) id: number,
    @Args('updateServiceInput') updateServiceInput: UpdateServiceInput,
  ): Promise<Service> {
    return await this.servicesService.update(id, updateServiceInput);
  }


  @Mutation(() => String)
  @Roles('employee')
  @UseGuards(JwtAuthGuard, RolesGuard)
  async removeService(
    @Args('ids', { type: () => [Int] }) ids: number[],
  ): Promise<string> {
    return await this.servicesService.remove(ids);
  }
}
