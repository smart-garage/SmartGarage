import { ObjectType, Field, Int } from '@nestjs/graphql';
import { Report } from 'src/reports/entities/report.entity';
import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';

@ObjectType()
@Entity()
export class Service {
  @PrimaryGeneratedColumn()
  @Field(() => Int)
  id: number;

  @Field()
  @Column()
  name: string;

  @Field()
  @Column({ type: 'float' })
  price: number;

  @ManyToMany(() => Report, report => report.services)
  reports: Report[];
}
