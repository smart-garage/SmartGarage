import { Test, TestingModule } from '@nestjs/testing';
import { ServicesResolver } from './services.resolver';
import { ServicesService } from './services.service';

describe('ServicesResolver', () => {
  let resolver: ServicesResolver;

  const mockServicesService = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ServicesResolver, ServicesService],
    })
      .overrideProvider(ServicesService)
      .useValue(mockServicesService)
      .compile();

    resolver = module.get<ServicesResolver>(ServicesResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
