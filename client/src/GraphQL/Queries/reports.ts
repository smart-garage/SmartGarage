import { gql } from "@apollo/client";

export const REPORTS = gql`
  query GetAllReports($customerId: Int!) {
    reports(customerId: $customerId) {
      id
      date
      customer {
        firstName
        lastName
      }
      vehicle {
        manufacturer
        model
      }
      services {
        name
        price
      }
    }
  }
`;

export const GET_REPORT = gql`
  query GetReport($id: Int!) {
    report(id: $id) {
      date
      customer {
        id 
        firstName
        lastName
        email
        phone
      }
      vehicle {
        id
        manufacturer
	    model
        year
        vin
        registration_plate
      }
      services {
        id
        name
        price
      }
    }
  }
`;