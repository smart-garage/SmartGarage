import { gql } from '@apollo/client'

// Get all customers
export const LOAD_CUSTOMERS = gql`
    query LoadCustomers {
        customers {
            id
            email
            firstName
            lastName
            phone
        }
    }
`

// Get single customer by id
export const LOAD_CUSTOMER_BY_ID = gql`
    query LoadCustomerById($id: Int!) {
        customerById(id: $id) {
            id
            email
            firstName
            lastName
            phone
        }
    }
`

// Get customer by id (self)
export const LOAD_CUSTOMER_SELF_BY_ID = gql`
    query LoadCustomerSelfById($loggedCustomerId: Int!, $id: Int!) {
        getSelfCustomerById(
            loggedCustomerId: $loggedCustomerId, id: $id
            ) {
                id
                email
                firstName
                lastName
                phone
        }
    }
`

export const CUSTOMER_EMAIL = gql`
  query GetCustomerEmail {
    customers {
      id
      email
    }
  }
`;
