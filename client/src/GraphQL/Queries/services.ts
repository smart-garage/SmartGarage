import { gql } from "@apollo/client";

// Get all services
export const SERVICES = gql`
  query GetAllServices {
    services {
      id
      name
      price
    }
  }
`;

// Get a single service by id
export const SERVICE = gql`
  query GetService($id: Int!) {
    service {
      id
      name
      price
    }
  }
`;