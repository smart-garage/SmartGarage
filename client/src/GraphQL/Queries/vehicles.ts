import { gql } from "@apollo/client";

export const VEHICLES = gql`
query GetAllVehicles {
  vehicles {
    id
    manufacturer
    model
    year
    vin
    registration_plate
    customer {
      email
    }
  }
}
`;

export const VEHICLES_LICENSE_PLATE = gql`
query GetVehiclesLicensePlate {
  vehicles {
    id
    registration_plate
  }
}
`;