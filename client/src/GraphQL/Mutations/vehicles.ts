import { gql } from "@apollo/client";

export const DELETE_VEHICLE = gql`
  mutation ($id: Int!) {
    removeVehicle(id: $id)
  }
`;

export const CREATE_VEHICLE = gql`
  mutation ($input: CreateVehicleInput!) {
    createVehicle(createVehicleInput: $input) {
      id
    }
  }
`;

export const UPDATE_VEHICLE = gql`
  mutation ($id: Int!, $input: UpdateVehicleInput!) {
    updateVehicle(id: $id, updateVehicleInput: $input) {
      id
    }
  }
`;