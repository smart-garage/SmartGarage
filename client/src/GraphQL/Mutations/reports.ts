import { gql } from "@apollo/client";

export const CREATE_REPORT = gql`
  mutation CreateReport($input: CreateReportInput!){
  createReport(createReportInput: $input) {
    id
  }
}
`;

export const FIND_VEHICLE_BY_LICENSE_PLATE = gql`
query findByPlate($vehicle: String!){
    vehicleByLicensePlate(vehicle: $vehicle) {
      id
    }
}
`;

export const CUSTOMER_BY_EMAIL = gql`
query CustomerByMail($email: String!){
    customerByMail(email: $email) {
      id
    }
}
`;