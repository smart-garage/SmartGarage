import { gql } from "@apollo/client";

// Create/register a customer
export const REGISTER_CUSTOMER = gql`
  mutation RegisterCustomer(
    $email: String!
    $firstName: String!
    $lastName: String!
    $phone: String!
  ) {
    createCustomer(
      createCustomerInput: {
        email: $email
        firstName: $firstName
        lastName: $lastName
        phone: $phone
      }
    ) {
      firstName
      lastName
      email
      phone
    }
  }
`;
// return id?

// Update customer
export const UPDATE_CUSTOMER = gql`
  mutation UpdateCustomer($input: UpdateCustomerInput!, $id: Int!) {
    updateCustomer(updateCustomerInput: $input, id: $id) {
      id
    }
  }
`;

// Delete customer
export const DELETE_CUSTOMER = gql`
  mutation DeleteCustomer($id: Int!) {
    removeCustomer(id: $id) {
      id
      email
      firstName
      lastName
      phone
    }
  }
`;

// Customer login
export const LOGIN_CUSTOMER = gql`
  mutation LoginCustomer($email: String!, $password: String!) {
    loginCustomer(loginInput: { email: $email, password: $password })
  }
`;

// Send password reset link
export const FORGOTTEN_PASS_LINK = gql`
  mutation ForgotPasswordCustomer($email: String!) {
    forgotPasswordCustomer(email: $email)
  }
`;

// Change forgotten password
export const CHANGE_FORGOTTEN_PASS = gql`
  mutation ChangeForgottenPasswordCustomer(
    $token: String!
    $password: String!
  ) {
    changeForgottenPasswordCustomer(
      changeForgottenPasswordInput: { token: $token, password: $password }
    )
  }
`;

// Change password
export const CHANGE_PASSWORD = gql`
  mutation ChangePassword($password: String!) {
    changePassword(changePasswordInput: { password: $password })
  }
`;
