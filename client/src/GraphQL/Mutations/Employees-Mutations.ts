import { gql } from "@apollo/client"

// Employee login
export const LOGIN_EMPLOYEE = gql`
    mutation LoginEmployee(     
        $email: String!, 
        $password: String!
        ){
        loginEmployee(loginInput: {
            email: $email,
            password: $password,
        })
}
`
