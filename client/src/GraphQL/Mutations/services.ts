import { gql } from "@apollo/client";

export const UPDATE_SERVICE = gql`
  mutation UpdateService($id: Int!, $input: UpdateServiceInput!) {
    updateService(id: $id, updateServiceInput: $input) {
      id
      name
      price
    }
  } 
`;

export const DELETE_SERVICE = gql`
  mutation DeleteService($ids: [Int!]!) {
    removeService(ids: $ids)
  } 
`;

export const CREATE_SERVICE = gql`
  mutation CreateService($input: CreateServiceInput!) {
    createService(createServiceInput: $input) {
      id
    }
  } 
`;