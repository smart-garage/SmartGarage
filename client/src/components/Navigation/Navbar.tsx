import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import { NavLink } from 'react-router-dom';
import AuthContext from '../../providers/AuthContext';
import wLogo from '../../images/logo-rainbow-girl.png';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexShrink: 1,
    paddingRight: 24,
  },
  bar: {
    background: '#11001a'
  }
}));

export default function MenuAppBar() {
  const classes = useStyles();
  const auth = useContext(AuthContext);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleMenu = (event: any) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const logout = () => {
    localStorage.removeItem('token');

    auth.setAuthState({
      user: null,
      isLoggedIn: false,
    });
  }

  return (
    <div className={classes.root}>
      <AppBar className={classes.bar} position="sticky">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <img src={wLogo} alt={"logo"} style={{ maxWidth: "2.5vw" }} />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            <NavLink to="/home" style={{ textDecoration: 'none', color: 'white' }} activeStyle={{ color: '#f09800' }}>
              Home
          </NavLink>
          </Typography>
          {auth.authValue.isLoggedIn === true && (
            <Typography variant="h6" className={classes.title}>
            <NavLink to="/services" style={{ textDecoration: 'none', color: 'white' }} activeStyle={{ color: '#f09800' }}>
                Services
            </NavLink>
            </Typography>)}
            {auth.authValue.isLoggedIn === true && (
            <Typography variant="h6" className={classes.title}>
            <NavLink to="/vehicles" style={{ textDecoration: 'none', color: 'white' }} activeStyle={{ color: '#f09800' }}>
                Vehicles
            </NavLink>
            </Typography>)}
            {auth.authValue.isLoggedIn === true && (
            <Typography variant="h6" className={classes.title}>
            <NavLink to="/customers" style={{ textDecoration: 'none', color: 'white' }} activeStyle={{ color: '#f09800' }}>
                Customers
            </NavLink>
            </Typography>)}

          {auth.authValue.isLoggedIn === false && (<Typography variant="h6" className={classes.title}>
            <NavLink to="/login/customer" style={{ textDecoration: 'none', color: 'white' }} activeStyle={{ color: '#f09800' }}>
              Login
          </NavLink>
          </Typography>)}
          {auth.authValue.isLoggedIn && (
            <div>
              <IconButton
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                color="inherit"
              >
                <AccountCircle />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={open}
                onClose={handleClose}
              >
                <NavLink to="/profile" style={{ textDecoration: 'none', color: 'black'}}><MenuItem>Profile</MenuItem></NavLink>
                <MenuItem onClick={()=> logout()}>Log Out</MenuItem>
              </Menu>
            </div>
          )}
        </Toolbar>
      </AppBar>
    </div>
  );
}
