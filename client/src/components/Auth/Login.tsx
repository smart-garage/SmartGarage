import React, { useContext, useState } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
// import FormControlLabel from '@material-ui/core/FormControlLabel';
// import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
// import Box from '@material-ui/core/Box';
// import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { Formik, Field, Form } from "formik";
import { LOGIN_CUSTOMER } from '../../GraphQL/Mutations/Customers-Mutations';
import { useMutation } from '@apollo/client';
import * as Yup from 'yup';
import decode from 'jwt-decode';
import { RouteComponentProps } from "react-router-dom";
import AuthContext from '../../providers/AuthContext';
import { LOGIN_EMPLOYEE } from '../../GraphQL/Mutations/Employees-Mutations';
import cLogo from '../../images/logo-rainbow-boy.png';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

type TParams = { id: string };

const Login = ({ history, match, location }: RouteComponentProps<TParams>) => {
    const auth = useContext(AuthContext);
    const classes = useStyles();
    const [loginCustomer, { error: custMutationError }] = useMutation(LOGIN_CUSTOMER);
    const [loginEmployee, { error: empMutationError }] = useMutation(LOGIN_EMPLOYEE);
    const [credentialError, setCredentialError] = useState(null);

    const LoginSchema = Yup.object().shape({
        email: Yup.string()
            .required('Required'),
        password: Yup.string()
            .required('Required'),
    });

    
    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>    
                <img src={cLogo} alt={"logo"} style={{ maxWidth: "3vw"}}/>
                {(location.pathname.includes("customer")) ? 
                (<Typography component="h1" variant="h5">
                    Customer Login
                </Typography>) :
                (<Typography component="h1" variant="h5">
                    Employee Login
                </Typography>)
                }
                <Formik
                    initialValues={{ email: "", password: "" }}
                    validationSchema={LoginSchema}
                    onSubmit={async (values, { setSubmitting }) => {
                        setSubmitting(true)
                        try {
                            // for customer/employee differentiation - get the location
                            if (location.pathname.includes("customer")) {
                                const access_token = await loginCustomer({
                                    variables: {
                                        email: values.email,
                                        password: values.password,
                                    }
                                })
                                const token = access_token.data.loginCustomer;
                                const user = decode(token);
                                localStorage.setItem('token', token)
                                auth.setAuthState({ user, isLoggedIn: true });
                            } else if (location.pathname.includes("employee")) {
                                const access_token = await loginEmployee({
                                    variables: {
                                        email: values.email,
                                        password: values.password,
                                    }
                                })
                                const token = access_token.data.loginEmployee;
                                const user = decode(token);
                                localStorage.setItem('token', token)
                                auth.setAuthState({ user, isLoggedIn: true });
                            }
                            history.push('/home'); // to redirect to initially targeted page?
                        } catch (error) {
                            setCredentialError(error);
                        }
                        setSubmitting(false)
                    }} >
                    {({ values, errors, touched, isSubmitting }) => (
                        <Form className={classes.form}>
                            <div>   <Field name="email" placeholder="e-mail" type="input" as={TextField} /></div>
                            {errors.email && touched.email && <div>{errors.email}</div>}
                            <div>   <Field name="password" placeholder="password" type="password" as={TextField} /></div>
                            {errors.password && touched.password && <div>{errors.password}</div>}
                            {credentialError && <div>Invalid credentials!</div>}
                            <Button
                                type="submit"
                                disabled={isSubmitting}
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                            > Log In </Button>
                            <Grid container>
                                <Grid item xs>
                                    <Link href="/forgotten-password" variant="body2">
                                        Forgot password?
                                    </Link>
                                </Grid>
                                <Grid item xs>
                                    {location.pathname.includes("customer") ?
                                        (<Link href="/login/employee" variant="body2">
                                            Employee login
                                        </Link>)
                                        : (<Link href="/login/customer" variant="body2">
                                            Customer login
                                        </Link>)}
                                </Grid>
                            </Grid>
                        </Form>)}
                </Formik>
            </div>
        </Container>
    );
}

export default Login;
