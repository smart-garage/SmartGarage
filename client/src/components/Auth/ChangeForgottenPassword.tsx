import React from 'react';
import { useMutation } from "@apollo/client";
import { CHANGE_FORGOTTEN_PASS } from "../../GraphQL/Mutations/Customers-Mutations";
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { Formik, Field, Form } from "formik";
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import MailOutlinedIcon from '@material-ui/icons/MailOutlined';
import * as Yup from 'yup';
import { useState } from "react";
import { useHistory } from "react-router-dom";

import { createStyles, Theme, withStyles, WithStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { DialogContentText } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const styles = (theme: Theme) =>
    createStyles({
        root: {
            margin: 0,
            padding: theme.spacing(2),
        },
        closeButton: {
            position: 'absolute',
            right: theme.spacing(1),
            top: theme.spacing(1),
            color: theme.palette.grey[500],
        },
    });

export interface DialogTitleProps extends WithStyles<typeof styles> {
    id: string;
    children: React.ReactNode;
    onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
    const { children, classes, onClose, ...other } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles((theme: Theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
    },
}))(MuiDialogActions);

const ResetPasswordSchema = Yup.object().shape({
    password: Yup.string()
        .required('Required')
        .min(8, 'Too Short!'),
    confirmPassword: Yup.string()
        .required('Required')
});

const validateConfirmPassword = (password:string, confirmPassword:string) => {
    let error = "";
    if (password && confirmPassword) {
        if (password !== confirmPassword) {
            error = "Passwords do not match!";
        }
    }
    return error;
};

const ChangeForgottenPassword = (props: { token: string }) => {
    let history = useHistory();

    const [openDialog, setOpen] = React.useState(false);
    const openConfirmDialog = () => {
        setOpen(true);
    };
    const closeDialog = () => {
        setOpen(false);
        history.push("/home")
    };

    const [changePass, { error: mutationError }] = useMutation(CHANGE_FORGOTTEN_PASS, {
        onCompleted: () => openConfirmDialog(),
    });
    const [resetError, setResetError] = useState(null);
    const classes = useStyles();


    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <MailOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Forgotten password
            </Typography>
                <Formik
                    initialValues={{ token: "", password: "", confirmPassword:"" }}
                    validationSchema={ResetPasswordSchema}
                    onSubmit={async (values, { setSubmitting }) => {
                        setSubmitting(true)
                        try {
                            changePass({ variables: { token: props.token, password: values.password } })
                        } catch (error) {
                            setResetError(error);
                        }
                        setSubmitting(false)
                    }} >
                    {({ values, errors, touched, isSubmitting }) => (
                        <Form className={classes.form}>
                            <div>   <Field name="password" placeholder="password" type="password" as={TextField} /></div>
                            {errors.password && touched.password && <div>{errors.password}</div>}
                            <div>   <Field name="confirmPassword" placeholder="confirm password" type="password" validate={(value:string) =>
                                validateConfirmPassword(values.password, value)} as={TextField} /></div>
                            {errors.confirmPassword && <div>{errors.confirmPassword}</div>}
                            {/* check for mutation error */}
                            {/* REPEAT PASS TO CHECK? */}
                            {resetError && <div>Unexpected error!</div>}
                            <Button
                                type="submit"
                                disabled={isSubmitting}
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                            > Reset password </Button>

                        </Form>)}
                </Formik>
                {openDialog && (<Dialog onClose={closeDialog} aria-labelledby="customized-dialog-title" open={openDialog}>
                    <DialogTitle id="customized-dialog-title" onClose={closeDialog}>
                        Success!
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            Password successfully changed!
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button autoFocus onClick={closeDialog} color="primary">
                            OK
                        </Button>
                    </DialogActions>
                </Dialog>)}
            </div>
        </Container>
    )
}

export default ChangeForgottenPassword;
