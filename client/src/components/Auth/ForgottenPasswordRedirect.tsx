import React, { useEffect, useState } from "react";
import { useHistory, useLocation } from "react-router";
import ChangeForgottenPassword from "./ChangeForgottenPassword";

// to rewrite if time
const ForgottenPasswordRedirect = () => {
    const query1 = new URLSearchParams(window.location.search);
    const location = useLocation();
    const history = useHistory()
    const [token, setState] = useState("");

    useEffect(() => {
      const queryParams = new URLSearchParams(location.search)
  
      if (queryParams.has('token')) {
          
        const received_token = query1.get('token');
        if (!!received_token) {
            setState(received_token);
        }
        queryParams.delete("token")
        history.replace("/forgotten-password-reset");
      }
    }, []);

    return (
        token ? <ChangeForgottenPassword token={token}/>
            :  (<div>Redirecting...</div>)
    )
}

export default ForgottenPasswordRedirect;
