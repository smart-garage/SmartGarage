import { useMutation } from "@apollo/client";
import { FORGOTTEN_PASS_LINK } from "../../GraphQL/Mutations/Customers-Mutations";
import Container from '@material-ui/core/Container';
import { Formik, Field, Form } from "formik";
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import LockOpenOutlinedIcon from '@material-ui/icons/LockOpenOutlined';
import * as Yup from 'yup';
import { useState } from "react";

import React from 'react';
import { createStyles, Theme, withStyles, WithStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { DialogContentText } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const styles = (theme: Theme) =>
    createStyles({
        root: {
            margin: 0,
            padding: theme.spacing(2),
        },
        closeButton: {
            position: 'absolute',
            right: theme.spacing(1),
            top: theme.spacing(1),
            color: theme.palette.grey[500],
        },
    });

export interface DialogTitleProps extends WithStyles<typeof styles> {
    id: string;
    children: React.ReactNode;
    onClose: () => void;
}

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
    const { children, classes, onClose, ...other } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles((theme: Theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);

const DialogActions = withStyles((theme: Theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
    },
}))(MuiDialogActions);


const ForgottenPassword = () => {
    const [openDialog, setOpen] = React.useState(false);
    const handleClickOpenDialog = () => {
        setOpen(true);
    };
    const handleCloseDialog = () => {
        setOpen(false);
    };

    const [forgotPass, { error: mutationError }] = useMutation(FORGOTTEN_PASS_LINK, {
        onCompleted: () => handleClickOpenDialog(),
    });
    const [missingEmailError, setMissingEmailError] = useState(null);

    const classes = useStyles();

    const SendMailSchema = Yup.object().shape({
        email: Yup.string()
            .required('Required'),
    });

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOpenOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Forgotten password
                </Typography>
                <Formik
                    initialValues={{ email: "" }}
                    validationSchema={SendMailSchema}
                    onSubmit={async (values, { setSubmitting }) => {
                        setSubmitting(true)
                        try {
                            forgotPass({ variables: { email: values.email } })
                        } catch (error) {
                            setMissingEmailError(error);
                        }
                        setSubmitting(false)
                    }} >
                    {({ values, errors, touched, isSubmitting }) => (
                        <Form className={classes.form}>
                            <div>   <Field name="email" placeholder="e-mail" type="input" as={TextField} /></div>
                            {errors.email && touched.email && <div>{errors.email}</div>}
                            {missingEmailError && <div>E-mail not in system!</div>}
                            {/* check for mutation error */}
                            <Button
                                type="submit"
                                disabled={isSubmitting}
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                            > Send password reset e-mail </Button>

                        </Form>)}
                </Formik>
                {openDialog && (<Dialog onClose={handleCloseDialog} aria-labelledby="customized-dialog-title" open={openDialog}>
                    <DialogTitle id="customized-dialog-title" onClose={handleCloseDialog}>
                        Success!
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            An e-mail was sent to you with a link to reset your password!
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button autoFocus onClick={handleCloseDialog} color="primary">
                            OK
                        </Button>
                    </DialogActions>
                </Dialog>)}
            </div>
        </Container>
    )
}

export default ForgottenPassword;