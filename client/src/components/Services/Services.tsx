import React from "react";
import { useMutation, useQuery } from "@apollo/client";
import { SERVICES } from "../../GraphQL/Queries/services";
import {
  DELETE_SERVICE,
  UPDATE_SERVICE,
  CREATE_SERVICE,
} from "../../GraphQL/Mutations/services";
import {
  GridColDef,
  GridFilterModel,
  GridRowsProp,
  XGrid,
  LicenseInfo,
  GridCellParams,
  useGridApiRef,
  GridSelectionModelChangeParams,
} from "@material-ui/x-grid";
import EditToolBar from "./EditToolBar";
import { ServicesData } from "../../utils/types/service";
import CreateForm from "./CreateForm";
import { Button, Dialog, DialogTitle, DialogContent } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import CreateReport from "./CreateReport";

LicenseInfo.setLicenseKey(
  "b04c3d6853fe56a3fbf52d2a22210187T1JERVI6MjU2NTAsRVhQSVJZPTE2NTQ0OTY3MjYwMDAsS0VZVkVSU0lPTj0x"
);

const Services = () => {
  const { error, loading, data, refetch } = useQuery<ServicesData>(SERVICES);
  const [removeService] = useMutation(DELETE_SERVICE, {
    onCompleted: () => refetch(),
  });
  const [updateService] = useMutation(UPDATE_SERVICE);
  const [createService] = useMutation(CREATE_SERVICE, {
    onCompleted: () => refetch(),
  });

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const [reportOpen, setReportOpen] = React.useState(false);

  const handleReportClickOpen = () => {
    setReportOpen(true);
  };

  const handleReportClose = () => {
    setReportOpen(false);
  };

  const apiRef = useGridApiRef();
  const [selectedCellParams, setSelectedCellParams] =
    React.useState<GridCellParams | null>(null);

  const handleCellClick = React.useCallback((params: GridCellParams) => {
    setSelectedCellParams(params);
  }, []);

  const [selectedRow, setSelectedRow] =
    React.useState<GridSelectionModelChangeParams | null>(null);

  const handleRowClick = React.useCallback(
    (params: GridSelectionModelChangeParams) => {
      setSelectedRow(params);
    },
    []
  );

  const handleDoubleCellClick = React.useCallback(
    (params: GridCellParams, event: React.SyntheticEvent) => {
      event.stopPropagation();
    },
    []
  );

  // Prevent from rolling back on escape
  const handleCellKeyDown = React.useCallback(
    (params, event: React.KeyboardEvent) => {
      if (["Escape", "Delete", "Backspace", "Enter"].includes(event.key)) {
        event.stopPropagation();
      }
    },
    []
  );

  // Prevent from committing on blur
  const handleCellBlur = React.useCallback(
    (params, event?: React.SyntheticEvent) => {
      if (params.cellMode === "edit") {
        event?.stopPropagation();
      }
    },
    []
  );

  if (loading) {
    return <div>Loading...</div>;
  }

  const columns: GridColDef[] = [
    { field: "name", headerName: "Name", width: 150, editable: true },
    {
      field: "price",
      headerName: "Price",
      width: 150,
      type: "number",
      editable: true,
    },
  ];

  const filterModel: GridFilterModel = {
    items: [{ columnField: "name" }, { columnField: "price" }],
  };

  const rows: GridRowsProp = data!.services;

  return (
    <div style={{ height: 520, maxWidth: "100%" }}>
      <XGrid
        disableMultipleSelection={true}
        checkboxSelection={true}
        onSelectionModelChange={handleRowClick}
        pagination={true}
        pageSize={15}
        filterModel={filterModel}
        columns={columns}
        rows={rows}
        loading={rows.length === 0}
        rowHeight={38}
        apiRef={apiRef}
        onCellClick={handleCellClick}
        onCellDoubleClick={handleDoubleCellClick}
        onCellBlur={handleCellBlur}
        onCellKeyDown={handleCellKeyDown}
        components={{
          Toolbar: EditToolBar,
        }}
        componentsProps={{
          toolbar: {
            selectedCellParams,
            apiRef,
            setSelectedCellParams,
            selectedRow,
            updateService,
            removeService,
          },
        }}
      />
      <Button
        variant="outlined"
        color="primary"
        onClick={handleClickOpen}
        startIcon={<AddIcon />}
      >
        Add Service
      </Button>
      <Button
        variant="outlined"
        color="primary"
        onClick={handleReportClickOpen}
        startIcon={<AddCircleIcon />}
      >
        New Report
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Create a new service!</DialogTitle>
        <DialogContent>
          <CreateForm
            createService={createService}
            handleClose={handleClose}
          ></CreateForm>
        </DialogContent>
      </Dialog>
      <Dialog
        open={reportOpen}
        onClose={handleReportClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Create a new report!</DialogTitle>
        <CreateReport
          selectedRow={selectedRow}
          handleReportClose={handleReportClose}
        ></CreateReport>
        <DialogContent></DialogContent>
      </Dialog>
    </div>
  );
};

export default Services;
