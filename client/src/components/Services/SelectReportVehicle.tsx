import { useQuery } from "@apollo/client";
import { VEHICLES_LICENSE_PLATE } from "../../GraphQL/Queries/vehicles";
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { Vehicles } from "../../utils/types/vehicles";

interface props {
  setFieldValue: any;
}
const SelectReportVehicle = ({ setFieldValue }: props) => {
  const { loading, data } = useQuery<Vehicles>(VEHICLES_LICENSE_PLATE);

  if (loading) {
    return <div>Loading...</div> 
  }

  return (
    <Autocomplete
      id="combo-box-demo"
      options={data!.vehicles}
      onChange={(e, value) => {
        if(!value) {
          setFieldValue('vehicle', '');
          return;
        } 
        setFieldValue('vehicle', value!.registration_plate)}}
      getOptionLabel={(option) => option.registration_plate}
      style={{ width: 300, marginTop: '30px' }}
      renderInput={(params) => <TextField {...params} label="License Plate" variant="outlined" />}
    />
  );

}

export default SelectReportVehicle;