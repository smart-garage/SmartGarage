import { useLazyQuery, useMutation, useQuery } from "@apollo/client";
import { Button } from "@material-ui/core";
import { Form, Formik } from "formik";
import { CREATE_REPORT, FIND_VEHICLE_BY_LICENSE_PLATE, CUSTOMER_BY_EMAIL } from "../../GraphQL/Mutations/reports";
import { SERVICES } from "../../GraphQL/Queries/services";
import { VEHICLES } from "../../GraphQL/Queries/vehicles";
import CreateCustomer from "../Vehicles/CreateCustomer";
import SelectReportVehicle from "./SelectReportVehicle";

interface props {
  selectedRow: any;
  handleReportClose: any;
}

const CreateReport = ({ selectedRow, handleReportClose }: props) => {
    const {  loading, data } = useQuery(SERVICES);
    const [createReport] = useMutation(CREATE_REPORT);
    const [getCustomer, customerId] = useLazyQuery(CUSTOMER_BY_EMAIL);
    const [getVehicleId, vehicleId] = useLazyQuery(FIND_VEHICLE_BY_LICENSE_PLATE);

     if (loading || customerId.loading || vehicleId.loading) {
      return <div>Loading...</div>;
    }
    //@ts-ignore
    const servicesSelected = data.services.filter(service => {
      if (selectedRow.selectionModel.includes(service.id)) {
        return true;
      }
    //@ts-ignore
    }).map(service => service.name);
  return (
    <div>
      <Formik
        initialValues={{
          email: '',
          vehicle: '',
        }}
        onSubmit={async (values, { setSubmitting }) => {
          setSubmitting(true);
          console.log(values.email);
          const main = async() => {
            await getCustomer({ variables: { email: values.email }});
            await getVehicleId({ variables: { vehicle: values.vehicle }})
            if (customerId && vehicleId) {
              const createReportInput = {
              vehicleId: vehicleId.data!,
              customerId: customerId.data!,
              servicesIds: selectedRow.selectionModel,
            }
            return createReportInput;
          }
        }
          const createReportInput = await main();
          const newCreateReportInput = {
            customerId: createReportInput!.customerId.customerByMail.id,
            vehicleId: createReportInput!.vehicleId.vehicleByLicensePlate.id,
            servicesIds: createReportInput!.servicesIds
          }
          await console.log(newCreateReportInput);
          await createReport({ variables: { input: newCreateReportInput }});
          setSubmitting(false);
        }}
      >
        {({ values, isSubmitting, setFieldValue }) => (
          <Form style={{ marginLeft: '30px' }}>
              <h3>The report contains the following services: {servicesSelected.join(', ')}</h3>
            <CreateCustomer setFieldValue={setFieldValue}></CreateCustomer>
            <SelectReportVehicle setFieldValue={setFieldValue}></SelectReportVehicle>
            <Button disabled={isSubmitting} onClick={handleReportClose}>
              Cancel!
            </Button>
            <Button disabled={isSubmitting} type="submit">
              Submit!
            </Button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default CreateReport;
