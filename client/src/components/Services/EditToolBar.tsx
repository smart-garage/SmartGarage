import React from "react";
import {
  FetchResult,
  MutationFunctionOptions,
  OperationVariables,
} from "@apollo/client";
import * as Yup from "yup";
import DeleteIcon from "@material-ui/icons/Delete";
import {
  GridApiRef,
  GridSelectionModelChangeParams,
} from "@material-ui/x-grid";
import {
  Button,
  createMuiTheme,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  makeStyles,
  Snackbar,
  Theme,
} from "@material-ui/core";
import MuiAlert, { AlertProps } from "@material-ui/lab/Alert";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from "@material-ui/icons/Save";

function Alert(props: AlertProps) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const validationSchema = Yup.object().shape({
  name: Yup.string().required("Required!"),
  price: Yup.number()
    .required("Required!")
    .moreThan(0, "Please enter a valid price!"),
});

const defaultTheme = createMuiTheme();
const useStyles = makeStyles(
  (theme: Theme) => ({
    root: {
      justifyContent: "center",
      display: "flex",
      borderBottom: `1px solid ${theme.palette.divider}`,
    },
  }),
  { defaultTheme }
);

interface EditToolbarProps {
  apiRef: GridApiRef;
  setSelectedCellParams: (value: any) => void;
  selectedCellParams: any;
  selectedRow: GridSelectionModelChangeParams | null;
  updateService: any;
  removeService: (
    options?: MutationFunctionOptions<any, OperationVariables> | undefined
  ) => Promise<FetchResult<any, Record<string, any>, Record<string, any>>>;
}

let snackBarError = "";

const EditToolBar = (props: EditToolbarProps) => {
  const {
    selectedCellParams,
    apiRef,
    setSelectedCellParams,
    updateService,
    removeService,
    selectedRow,
  } = props;
  const classes = useStyles();

  // pass isEmolyee as prop and if not, return null

  const [open, setOpen] = React.useState(false);
  const ids = selectedRow?.selectionModel;

  const handleSnackBarClick = () => {
    setOpen(true);
  };

  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  const handleClick = () => {
    if (!selectedCellParams) {
      return;
    }
    const { id, field, cellMode } = selectedCellParams;
    const { name, price } = selectedCellParams.row;
    if (cellMode === "edit") {
      const editedCellProps = apiRef.current.getEditCellPropsParams(id, field);
      apiRef.current.setCellMode(id, field, "view");
      const input = { name, price: +price };
      // change the input based on which field we are editing
      selectedCellParams.field === "name"
        ? (input.name = editedCellProps.props.value)
        : (input.price = +editedCellProps.props.value!);
      // validation
      validationSchema
        .validate(input)
        .then(() => updateService({ variables: { id, input } }))
        .then(() => apiRef.current.commitCellChange(editedCellProps))
        .catch((err) => {
          handleSnackBarClick();
          snackBarError = err.message;
        })
        .finally(() =>
          setSelectedCellParams({ ...selectedCellParams, cellMode: "view" })
        );
    } else {
      apiRef.current.setCellMode(id, field, "edit");
      setSelectedCellParams({ ...selectedCellParams, cellMode: "edit" });
    }
  };

  const handleMouseDown = (event: React.SyntheticEvent) => {
    // Keep the focus in the cell
    event.preventDefault();
  };

  // these handle the delete dialog
  const [deleteOpen, setDeleteOpen] = React.useState(false);

  const handleDeleteOpen = () => {
    setDeleteOpen(true);
  };

  const handleDeleteClose = () => {
    setDeleteOpen(false);
  };

  const handleDelete = (event: React.SyntheticEvent) => {
    event.preventDefault();

    handleDeleteClose();
    removeService({ variables: { ids } });
  };

  return (
    <div className={classes.root}>
      <Button
        variant="contained"
        color="primary"
        style={{margin:"1vw"}}
        onClick={handleClick}
        onMouseDown={handleMouseDown}
        disabled={!selectedCellParams}
        startIcon={
          selectedCellParams?.cellMode === "edit" ? <SaveIcon /> : <EditIcon />
        }
      >
        {selectedCellParams?.cellMode === "edit" ? "Save" : "Edit"}
      </Button>
      <Button
        variant="contained"
        color="secondary"
        style={{margin:"1vw"}}
        onClick={handleDeleteOpen}
        onMouseDown={handleMouseDown}
        startIcon={<DeleteIcon />}
      >
        DELETE
      </Button>
      <Dialog open={deleteOpen} onClose={handleDeleteClose}>
        <DialogTitle id="alert-dialog-title">
          {"Are you sure you want to delete these services?"}
        </DialogTitle>
        <DialogContent>
          {/* <DialogContentText id="alert-dialog-description">
            </DialogContentText> */}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDeleteClose} color="primary">
            No, go back!
          </Button>
          <Button onClick={handleDelete} color="primary" autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>

      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          {snackBarError}
        </Alert>
      </Snackbar>
    </div>
  );
};

export default EditToolBar;
