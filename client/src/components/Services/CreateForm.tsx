import React from "react";
import { Button, TextField } from "@material-ui/core";
import { Field, FieldAttributes, Form, Formik, useField } from "formik";
import * as Yup from "yup";
import {
  MutationFunctionOptions,
  OperationVariables,
  FetchResult,
} from "@apollo/client";

const validationSchema = Yup.object().shape({
  name: Yup.string().required("Required!"),
  price: Yup.number()
    .required("Required!")
    .moreThan(0, "Please enter a valid price!"),
});

const MyTextField: React.FC<FieldAttributes<{}>> = ({
  placeholder,
  ...props
}) => {
  const [field, meta] = useField<{}>(props);
  const errorText = meta.error && meta.touched ? meta.error : "";
  return (
    <TextField
      placeholder={placeholder}
      {...field}
      helperText={errorText}
      error={!!errorText}
    />
  );
};

interface props {
  createService: (
    options?: MutationFunctionOptions<any, OperationVariables> | undefined
  ) => Promise<FetchResult<any, Record<string, any>, Record<string, any>>>;
  handleClose: any;
}

const CreateForm = ({ createService, handleClose }: props) => {
  return (
    <div>
      <Formik
        initialValues={{
          name: "",
          price: 0,
        }}
        validationSchema={validationSchema}
        onSubmit={(values, { setSubmitting }) => {
          setSubmitting(true);
          try {
            createService({ variables: { input: values } });
            handleClose();
          } catch (error) {
            return error;
          }
          setSubmitting(false);
        }}
      >
        {({ values, isSubmitting }) => (
          <Form>
            <div>
              <MyTextField placeholder="Name" name="name" />
            </div>
            <div>
              <Field placeholder="Price" name="price" type="number" as={TextField}/>
            </div>
            <Button disabled={isSubmitting} onClick={handleClose}>
              Cancel!
            </Button>
            <Button disabled={isSubmitting} type="submit">
              Submit!
            </Button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default CreateForm;
