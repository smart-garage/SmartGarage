import React, { useContext } from 'react';
import { Button, Container, Typography, Grid, Box } from '@material-ui/core'
import heroLarge from '../../images/hero-large.jpg';
import about from '../../images/about.png';
import location from '../../images/location.png';
import contact from '../../images/contact.png';
import anatLarge from '../../images/anat-large.jpg';
import { NavLink } from 'react-router-dom';
import airConditioning from '../../images/air-conditioning.png';
import wheelAlignment from '../../images/wheel-alignment.png';
import painting from '../../images/painting.png';
import steeringWheel from '../../images/steering-wheel.png';
import { makeStyles } from '@material-ui/core/styles';
import AuthContext from '../../providers/AuthContext';


const useStyles = makeStyles((theme) => ({
    smallCard: {
        margin: "2vw",
        background: "white",
        width: "10vw",
        height: "14vw",
        padding: "0 10px",
        boxShadow: "0 4px 8px 0 rgba(0,0,0,0.2)",
        transition: "0.3s",
        borderRadius: "5px"
    },
    card: {
        margin: "2vw",
        background: "white",
        width: "12vw",
        height: "16vw",
        padding: "2vw",
        boxShadow: "0 4px 8px 0 rgba(0,0,0,0.2)",
        transition: "0.3s",
        borderRadius: "5px"
    }
}))

const Homepage = (props: any) => {
    const auth = useContext(AuthContext);
    const classes = useStyles();

    return (
        <div>
            <div className="container" style={{ position: 'relative' }}>
                <div className="row">
                    <img src={heroLarge} alt={"GarageQL"} style={{ width: "100%" }} />

                    <div className="headers" style={{ position: 'absolute', top: '7vw', left: '9vw' }}>
                        <Typography component="h1" variant="h1" style={{ color: 'white' }}>GarageQL</Typography>
                        <Typography component="h3" variant="h4" style={{ color: 'white' }}>God-given service</Typography>
                    </div>
                    {!(auth.authValue.isLoggedIn) &&
                        (<NavLink to="/login/customer"
                            style={{ textDecoration: 'none' }}>
                            <Button variant="contained"
                                size="large"
                                style={{ position: 'absolute', top: '22vw', right: '32vw', color: "#11001a", background: '#f09800' }}>
                                Login
                            </Button></NavLink>)}

                    {/* </div> */}
                    {/* <div className="col-md-offset-1 col-md-10 col-sm-12 wow fadeInUp" data-wow-delay="0.3s"> */}
                    {/* <h1 className="wow fadeInUp" data-wow-delay="0.6s">Let's take a snapshot</h1>
                            <p className="wow fadeInUp" data-wow-delay="0.9s">Snapshot website template is available for free download. Anyone can modify and use it for any site. Please tell your friends about <a rel="nofollow" href="http://www.templatemo.com">templatemo</a>. Thank you.</p>
                            <a href="#about" className="smoothScroll btn btn-success btn-lg wow fadeInUp" data-wow-delay="1.2s">Learn more</a> */}
                    {/* </div> */}
                </div>
            </div>
            <h1>Services we offer</h1>
            <p>We offer a wide variety of services. Here are a few of them:</p>
            {/* <div style={{ maxWidth: "80vw", }}> */}
            <div className="row" style={{ display: "flex", alignItems: "center", justifyContent: "center", paddingBottom: "3vw", paddingTop: "1vw" }}>
                <div className={classes.smallCard}>
                    <img src={painting} alt={"painting"} style={{ maxWidth: "6vw", paddingTop: "2vw" }} />
                    <div className="container">
                        <h4><b>Repaint</b></h4>
                    </div>
                </div>
                <div className={classes.smallCard}>
                    <img src={wheelAlignment} alt={"Wheel alignment check"} style={{ maxWidth: "6vw", paddingTop: "2vw" }} />
                    <div className="container">
                        <h4><b>Wheel alignment check</b></h4>
                    </div>
                </div>
                <div className={classes.smallCard}>
                    <img src={steeringWheel} alt={"Steering wheel inspection"} style={{ maxWidth: "6vw", paddingTop: "2vw" }} />
                    <div className="container">
                        <h4><b>Steering wheel inspection</b></h4>
                    </div>
                </div>
                <div className={classes.smallCard}>
                    <img src={airConditioning} alt={"air conditioning"} style={{ maxWidth: "6vw", paddingTop: "2vw" }} />
                    <div className="container">
                        <h4><b>Air conditioning maintenance</b></h4>
                    </div>
                </div>
            </div>
            {/* </div> */}

            <img src={anatLarge} alt={"who are we"} style={{ width: "100%" }}
            // style={{width:"50vw"}}
            />
            <h1>Who we are</h1>
            <p>
                We are a team of dedicated professionals, young and old, who love their work and strive to make clients smile wider and cars run better.
                You can get acquainted with our founder Ivelin QL.

            </p>

            <div className="row" style={{ display: "flex", alignItems: "center", justifyContent: "center", padding: "1vw" }}>
                <div className={classes.card}>
                    <img src={contact} alt={"contact"} style={{ maxWidth: "8vw" }} />
                    <div className="container">
                        <h4><b>Get in touch</b></h4>
                        <p>Write us an e-mail at smartgaragebot@gmail.com.</p>
                    </div>
                </div>
                <div className={classes.card}>
                    <img src={location} alt={"location"} style={{ maxWidth: "8vw" }} />
                    <div className="container">
                        <h4><b>Location</b></h4>
                        <p>We are located on 18 Ivailo Street.</p>
                    </div>
                </div>
                <div className={classes.card}>
                    <img src={about} alt={"about"} style={{ maxWidth: "8vw" }} />
                    <div className="container">
                        <h4><b>Reviews</b></h4>
                        <p>Check out customer reviews!</p>
                    </div>
                </div>
            </div>
            <div>
            </div>
        </div >

    )
}

export default Homepage;
