import { useMutation, useQuery } from "@apollo/client";
import {
  GridCellParams,
  GridColDef,
  GridRowsProp,
  GridSelectionModelChangeParams,
  LicenseInfo,
  useGridApiRef,
  XGrid,
} from "@material-ui/x-grid";
import _ from "lodash";
import React from "react";
import { VEHICLES } from "../../GraphQL/Queries/vehicles";
import {
  DELETE_VEHICLE,
  UPDATE_VEHICLE,
} from "../../GraphQL/Mutations/vehicles";
import { Vehicles } from "../../utils/types/vehicles";
import EditToolbar from "./EditToolBar";


LicenseInfo.setLicenseKey(
  "b04c3d6853fe56a3fbf52d2a22210187T1JERVI6MjU2NTAsRVhQSVJZPTE2NTQ0OTY3MjYwMDAsS0VZVkVSU0lPTj0x"
);


const SingleVehicle = () => {
  const { error, loading, data, refetch } = useQuery<Vehicles>(VEHICLES);
  const [deleteVehicle] = useMutation(DELETE_VEHICLE, {
    onCompleted: () => refetch(),
  });

  const [updateVehicle] = useMutation(UPDATE_VEHICLE, { onCompleted: () => refetch() });

  const apiRef = useGridApiRef();


  const [selectedCellParams, setSelectedCellParams] = React.useState<GridCellParams | null>(null);

const handleCellClick = React.useCallback((params: GridCellParams) => {
  setSelectedCellParams(params);
}, []);


  const [selectedRow, setSelectedRow] =
    React.useState<GridSelectionModelChangeParams | null>(null);

  const handleRowClick = React.useCallback(
    (params: GridSelectionModelChangeParams) => {
      setSelectedRow(params);
    },
    []
  );

  // Prevent from rolling back on escape
  const handleCellKeyDown = React.useCallback(
    (params, event: React.KeyboardEvent) => {
      if (["Escape", "Delete", "Backspace", "Enter"].includes(event.key)) {
        event.stopPropagation();
      }
    },
    []
  );

  // Prevent from committing on blur
  const handleCellBlur = React.useCallback(
    (params, event?: React.SyntheticEvent) => {
      if (params.cellMode === "edit") {
        event?.stopPropagation();
      }
    },
    []
  );

  if (loading) {
    return <div>Loading...</div>;
  }

  const newData = data!.vehicles.map((vehicle) => {
    const newVehicle = _.cloneDeep(vehicle);
    newVehicle.customer = vehicle.customer.email;

    return newVehicle;
  });

  const rows: GridRowsProp = newData;

  const columns: GridColDef[] = [
    { field: "id", headerName: "ID", width: 150 },
    {
      field: "manufacturer",
      headerName: "Manufacturer",
      width: 150,
    },
    { field: "model", headerName: "Model", width: 150 },
    { field: "year", headerName: "Year", width: 110 },
    { field: "vin", headerName: "VIN", width: 190 },
    { field: "registration_plate", headerName: "License Plate", width: 180 },
    { field: "customer", headerName: "Customer", width: 180 },
  ];

  return (
    <div style={{ height: 520, maxWidth: "100%" }}>
      <XGrid
        disableMultipleSelection={true}
        onSelectionModelChange={handleRowClick}
        pagination={true}
        pageSize={15}
        columns={columns}
        rows={rows}
        loading={rows.length === 0}
        rowHeight={38}
        apiRef={apiRef}
        onCellClick={handleCellClick}
        onCellBlur={handleCellBlur}
        onCellKeyDown={handleCellKeyDown}
        components={{
          Toolbar: EditToolbar,
        }}
        componentsProps={{
          toolbar: {
            selectedRow,
            deleteVehicle,
            updateVehicle,
            selectedCellParams,
            setSelectedCellParams,
            apiRef,
          },
        }}
      />
    </div>
  );
};

export default SingleVehicle;
