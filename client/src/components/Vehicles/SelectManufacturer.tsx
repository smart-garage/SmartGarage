/* eslint-disable no-use-before-define */
import React from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete, {
  createFilterOptions,
} from "@material-ui/lab/Autocomplete";
import { gql, useQuery } from "@apollo/client";

const filter = createFilterOptions<VehicleOptionType>();

const VEHICLES_MANUFACTURERS = gql`
  query GetVehiclesManufacturers {
    vehicles {
      manufacturer
    }
  }  
`;

interface VehicleOptionType {
  inputValue?: string;
  manufacturer: string;
}

interface props {
  setFieldValue: any;
}

const SelectManufacturer = ({ setFieldValue }: props) => {
  const [value, setValue] = React.useState<VehicleOptionType | null>(null);
  const { error, loading, data } = useQuery(VEHICLES_MANUFACTURERS);

  return (
      <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
    <Autocomplete
      value={value}
      onChange={(event, newValue) => {
        if (typeof newValue === "string") {
            setValue({manufacturer: newValue});
            setFieldValue('manufacturer', newValue);
        } else if (newValue && newValue.inputValue) {
          // Create a new value from the user input
          setValue(newValue);
          setFieldValue('manufacturer' ,newValue.inputValue);
        } else {
            setValue(newValue);
            setFieldValue('manufacturer', newValue?.manufacturer);
        }
      }}
      filterOptions={(options, params) => {
        const filtered = filter(options, params);

        // Suggest the creation of a new value
        if (params.inputValue !== "") {
          filtered.push({
            inputValue: params.inputValue,
            manufacturer: `Add "${params.inputValue}"`,
          });
        }

        return filtered;
      }}
      selectOnFocus
      clearOnBlur
      handleHomeEndKeys
      id="manufacturer"
      //@ts-ignore
      options={[...new Map(data.vehicles.map(vehicle => [vehicle['manufacturer'], vehicle])).values()]}
      getOptionLabel={(option) => {
        // Value selected with enter, right from the input
        if (typeof option === "string") {
          return option;
        }
        // Add "xxx" option created dynamically
        if (option.inputValue) {
          return option.inputValue;
        }
        // Regular option
        return option.manufacturer;
      }}
      renderOption={(option) => option.manufacturer}
      style={{ width: 300 }}
      freeSolo
      renderInput={(params) => (
        <TextField
          {...params}
          name="manufacturer"
          label="Manufacturer"
          variant="outlined"
        />
      )}
    />
    </div>
  );
}

export default SelectManufacturer;