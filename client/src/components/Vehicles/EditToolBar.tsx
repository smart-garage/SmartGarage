import {
  MutationFunctionOptions,
  OperationVariables,
  FetchResult,
} from "@apollo/client";
import {
  Button,
  createMuiTheme,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  makeStyles,
  Snackbar,
  Theme,
} from "@material-ui/core";
import { GridApiRef, GridCellParams, GridSelectionModelChangeParams } from "@material-ui/x-grid";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import React from "react";
import MuiAlert, { AlertProps } from "@material-ui/lab/Alert";
import * as Yup from "yup";
import SaveIcon from "@material-ui/icons/Save";

const defaultTheme = createMuiTheme();
const useStyles = makeStyles(
  (theme: Theme) => ({
    root: {
      justifyContent: "center",
      display: "flex",
      borderBottom: `1px solid ${theme.palette.divider}`,
    },
  }),
  { defaultTheme }
);

function Alert (props: AlertProps) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
};

const validationSchema = Yup.object().shape({
  manufacturer: Yup.string().required("Required!"),
  model: Yup.string().required("Required!"),
  year: Yup.string()
    .required("Required!")
    .length(4, "Please enter a valid year!"),
  vin: Yup.string()
    .required("Required!")
    .length(17, "Please enter a valid vin!"),
  registration_plate: Yup.string()
    .required("Required!")
    .matches(/^[A-Z]{1,2}\d{4}[A-Z]{2}/, "Please enter a valid license plate!"),
});

interface ToolbarProps {
  selectedRow: GridSelectionModelChangeParams | null;
  deleteVehicle: (
    options?: MutationFunctionOptions<any, OperationVariables> | undefined
  ) => Promise<FetchResult<any, Record<string, any>, Record<string, any>>>;
  updateVehicle: (
    options?: MutationFunctionOptions<any, OperationVariables> | undefined
  ) => Promise<FetchResult<any, Record<string, any>, Record<string, any>>>;
  selectedCellParams: GridCellParams | null;
  setSelectedCellParams: any;
  apiRef: GridApiRef;
} 

let snackBarError = '';

const EditToolbar = (props: ToolbarProps) => {
  const classes = useStyles();

  const { selectedRow, deleteVehicle, updateVehicle, selectedCellParams, setSelectedCellParams, apiRef } = props;

  const handleMouseDown = (event: React.SyntheticEvent) => {
    // Keep the focus in the cell
    event.preventDefault();
  };

  const [open, setOpen] = React.useState(false);
  const id = selectedRow?.selectionModel[0];

  const handleSnackBarClick = () => {
    setOpen(true);
  };

  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  interface updateVehicleInput {
    manufacturer: string;
    model: string;
    year: string;
    vin: string;
    registration_plate: string;
  }

  const handleClick = () => {
    if (!selectedCellParams) {
      return;
    }
    const { id, field, cellMode } = selectedCellParams;
    const { manufacturer, model, year, vin, registration_plate } = selectedCellParams.row;
    if (cellMode === "edit") {
      const editedCellProps = apiRef.current.getEditCellPropsParams(id, field);
      apiRef.current.setCellMode(id, field, "view");
      const input: updateVehicleInput = { manufacturer, model, year, vin, registration_plate };
      // change the input based on which field we are editing
      const fieldToEdit = selectedCellParams.field;
      //@ts-ignore
      input[fieldToEdit] = editedCellProps.props.value;
      console.log(input);
      console.log(fieldToEdit);
      // validation 
      validationSchema
        .validate(input)
        .then(() => updateVehicle({ variables: { id: +id, input } }))
        .then(() => apiRef.current.commitCellChange(editedCellProps))
        .catch((err) => {
        // console.log(err.message);
          snackBarError = err.message;
          handleSnackBarClick();
        })
        .finally(() =>
          setSelectedCellParams({ ...selectedCellParams, cellMode: "view" })
        );
    } else {
      apiRef.current.setCellMode(id, field, "edit");
      setSelectedCellParams({ ...selectedCellParams, cellMode: "edit" });
    }
  };

  // these handle the delete dialog
  const [deleteOpen, setDeleteOpen] = React.useState(false);

  const handleDeleteOpen = () => {
    setDeleteOpen(true);
  };

  const handleDeleteClose = () => {
    setDeleteOpen(false);
  };

  const handleDelete = (event: React.SyntheticEvent) => {
    event.preventDefault();

    handleDeleteClose();
    deleteVehicle({ variables: { id } });
  };

  return (
    <div className={classes.root}>
      <Button
        variant="contained"
        color="primary"
        style={{margin:"1vw"}}
        disabled={!selectedRow}
        onMouseDown={handleMouseDown}
        onClick={handleClick}   
        startIcon={selectedCellParams?.cellMode === "edit" ? <SaveIcon /> : <EditIcon />}
      >
        {selectedCellParams?.cellMode === "edit" ? "Save" : "Edit"}
      </Button>
      <Button
        variant="contained"
        color="secondary"
        style={{margin:"1vw"}}
        disabled={!selectedRow}
        onMouseDown={handleMouseDown}
        onClick={handleDeleteOpen}
        startIcon={<DeleteIcon />}
      >
        Delete
      </Button>
      <Dialog open={deleteOpen} onClose={handleDeleteClose}>
        <DialogTitle id="alert-dialog-title">
          {"Are you sure you want to delete these services?"}
        </DialogTitle>
        <DialogContent>
          {/* <DialogContentText id="alert-dialog-description">
            </DialogContentText> */}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDeleteClose} color="primary">
            No, go back!
          </Button>
          <Button onClick={handleDelete} color="primary" autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>

      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          {snackBarError}
        </Alert>
      </Snackbar>
    </div>
  );
};

export default EditToolbar;
