import React from 'react';
import { useMutation, useQuery } from "@apollo/client";
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField } from "@material-ui/core";
import { Field, FieldAttributes, Form, Formik, useField } from "formik";
import SingleVehicle from "./SingleVehicle";
import { RouteComponentProps } from "react-router";
import { CREATE_VEHICLE } from '../../GraphQL/Mutations/vehicles';
import { VEHICLES } from '../../GraphQL/Queries/vehicles';
import { Vehicles } from '../../utils/types/vehicles';
import CreateForm from './CreateForm';
import AddIcon from '@material-ui/icons/Add';

type TParams = { id: string };


const AllVehicles = ({
  history,
  match,
  location,
}: RouteComponentProps<TParams>) => {
  const { loading, error, data, refetch } = useQuery<Vehicles>(VEHICLES);
  const [createVehicle] = useMutation(CREATE_VEHICLE, {
    onCompleted: () => refetch(),
  });
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };


  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    console.log(error); 
    return <div>Error {error.message}!</div>;
  }

  return (
    <div>
      <SingleVehicle></SingleVehicle>
      <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen} startIcon={<AddIcon />}>
        Add Vehicle
      </Button>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Create a new vehicle!</DialogTitle>
        <DialogContent>
          <CreateForm createVehicle={createVehicle} handleClose={handleClose}></CreateForm>
        </DialogContent>
      </Dialog>
    </div>
    </div>
  );
};

export default AllVehicles;
