import { gql, useMutation, useQuery } from "@apollo/client";
import { Button, TextField } from "@material-ui/core";
import { FieldAttributes, Form, Formik, useField } from "formik";
import React, { useState } from "react";
import { RouteComponentProps } from "react-router";
import * as Yup from "yup";

const VEHICLE = gql`
  query getVehicle($id: Int!) {
    vehicle(id: $id) {
      id
      manufacturer
      model
      year
      vin
      registration_plate
    }
  }
`;

const UPDATE_VEHICLE = gql`
  mutation ($id: Int!, $input: UpdateVehicleInput!) {
    updateVehicle(id: $id, updateVehicleInput: $input) {
      id
    }
  }
`;

interface VehicleData {
  manufacturer?: string;
  model?: string;
  year?: string;
  vin?: string;
  registration_plate?: string;
}

type TParams = { id: string };

const MyTextField: React.FC<FieldAttributes<{}>> = ({
  placeholder,
  ...props
}) => {
  const [field, meta] = useField<{}>(props);
  const errorText = meta.error && meta.touched ? meta.error : "";
  return (
    <TextField
      placeholder={placeholder}
      {...field}
      helperText={errorText}
      error={!!errorText}
    />
  );
};

const validationSchema = Yup.object().shape({
  manufacturer: Yup.string(),
  model: Yup.string(),
  year: Yup.string()
    .length(4, "Please enter a valid year!")
    .matches(/^\d+$/, "Please enter a valid year!"),
  vin: Yup.string()
    .length(17, "Please enter a valid vin!")
    .matches(/^\d+$/, "Please enter a valid vin!"),
  registration_plate: Yup.string()
    .matches(
      /^[A-Z]{1,2}\d{4}[A-Z]{2}/,
      "Please enter a valid license plate!"
    )
});


// filter out when field is an empty string 
// so it doesn't get sent in the mutation input
const filterValuesToUpdate = (data: VehicleData) => {
  const newData: Record<string, any> = {};
  const values = new Map(Object.entries(data));
  values.forEach((value, key) => {
    if(!value) {
      values.delete(key)
      return;
    }
    newData[key] = value;
  });

  return newData;
}


const Vehicle = ({ history, match, location }: RouteComponentProps<TParams>) => {
  const id = +match.params.id;
  const { error, loading, data, refetch } = useQuery(VEHICLE, { variables: { id } });
  const [updateVehicle] = useMutation(UPDATE_VEHICLE, { onCompleted: () => refetch() });
  const [hideForm, setHideForm] = useState(true);

  if (loading) {
    return <div>Loading...</div>
  }

  if (error) {
    return <div>Error {error.message}!</div>
  }

  return (
    <div>
      {data &&
          <div>
            <div>Manufacturer: {data.vehicle.manufacturer}</div>
            <div>Model: {data.vehicle.model}</div>
            <div>Year: {data.vehicle.year}</div>
            <div>Vin: {data.vehicle.vin}</div>
            <div>License Plate: {data.vehicle.registration_plate}</div>
            <Button onClick={() => setHideForm(!hideForm)}>Update Vehicle!</Button>
            {hideForm && <Formik
        initialValues={{  // can also make those an empty string
          manufacturer: data.vehicle.manufacturer,
          model: data.vehicle.model,
          year: data.vehicle.year,
          vin: data.vehicle.vin,
          registration_plate: data.vehicle.registration_plate,
        }}
        validationSchema={validationSchema}
        onSubmit={(values: VehicleData, { setSubmitting, resetForm }) => {
          setSubmitting(true);
          // make async call 
          const input = filterValuesToUpdate(values);
          updateVehicle({ variables: { id: data.vehicle.id, input } });
          setHideForm(false);
          setSubmitting(false);
        }}
      >
        {({ values, errors, touched, isSubmitting }) => (
          <Form>
            <div>
              <MyTextField placeholder="manufacturer" name="manufacturer"></MyTextField>
            </div>
            <div>
              <MyTextField placeholder="model" name="model"></MyTextField>
            </div>
            <div>
              <MyTextField placeholder="year" name="year"></MyTextField>
            </div>
            <div>
              <MyTextField placeholder="vin" name="vin" ></MyTextField>
            </div>
            <div>
              <MyTextField placeholder="license plate" name="registration_plate"></MyTextField>
            </div>
            <Button disabled={isSubmitting} type="submit">
              Submit!
            </Button>
            <pre>{JSON.stringify(values, null, 2)}</pre>
          </Form>
        )}
      </Formik>}
          </div>
        }
    </div>
  );
};

export default Vehicle;
