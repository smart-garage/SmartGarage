/* eslint-disable no-use-before-define */
import TextField from "@material-ui/core/TextField";
import Autocomplete, {
} from "@material-ui/lab/Autocomplete";
import { useQuery } from "@apollo/client";
import { CUSTOMER_EMAIL } from '../../GraphQL/Queries/customers'

interface NewCustomer {
  email: string;
}

interface Customer {
  email: string;
}

interface CustomersData {
  customers: Customer[];
}

interface props {
  setFieldValue: any;
}

const CreateCustomer = ({ setFieldValue }: props) => {
  const { loading, data } = useQuery<CustomersData>(CUSTOMER_EMAIL);

  if (loading) {
    return <div>Loading...</div>
  }
  
  const newData = data!.customers.map(customer => {
    const newPerson: NewCustomer = { email: '' };
    newPerson.email = customer.email;

    return newPerson;
  })

  const options = newData.map((customer) => {
    const firstLetter = customer.email[0].toUpperCase();
    return {
      firstLetter: /[0-9]/.test(firstLetter) ? '0-9' : firstLetter,
      ...customer,
    };
  });

  return (
<Autocomplete
  id="grouped-demo"
  onChange={(e, value) => {
    if(!value) {
      setFieldValue('email', '');
      return;
    } 
    setFieldValue('email', value!.email)}}
  options={options.sort((a, b) => -b.firstLetter.localeCompare(a.firstLetter))}
  groupBy={(option) => option.firstLetter}
  getOptionLabel={(option) => option.email}
  style={{ width: 300 }}
  renderInput={(params) => <TextField {...params} label="Customer" variant="outlined"/>}
/>
  );
}


export default CreateCustomer;