import React from "react";
import { Button, TextField } from "@material-ui/core";
import { FieldAttributes, Form, Formik, useField } from "formik";
import CreateCustomer from "../Vehicles/CreateCustomer";
import * as Yup from "yup";
import SelectManufacturer from "./SelectManufacturer";
import { MutationFunctionOptions, OperationVariables, FetchResult } from "@apollo/client";

const validationSchema = Yup.object().shape({
  manufacturer: Yup.string().required("Required!"),
  model: Yup.string().required("Required!"),
  year: Yup.string()
    .required("Required!")
    .length(4, "Please enter a valid year!")
    .matches(/^\d+$/, "Please enter a valid year!"),
  vin: Yup.string()
    .required("Required!")
    .length(17, "Please enter a valid vin!"),
  // .matches(/^\d+$/, "Please enter a valid vin!"),
  registration_plate: Yup.string()
    .required("Required!")
    .matches(/^[A-Z]{1,2}\d{4}[A-Z]{2}/, "Please enter a valid license plate!"),
});

const MyTextField: React.FC<FieldAttributes<{}>> = ({
  placeholder,
  ...props
}) => {
  const [field, meta] = useField<{}>(props);
  const errorText = meta.error && meta.touched ? meta.error : "";
  return (
    <TextField
      placeholder={placeholder}
      {...field}
      helperText={errorText}
      error={!!errorText}
    />
  );
};

interface props {
  createVehicle: (
    options?: MutationFunctionOptions<any, OperationVariables> | undefined
  ) => Promise<FetchResult<any, Record<string, any>, Record<string, any>>>;
  handleClose: any;
}

const CreateForm = ({ createVehicle, handleClose }: props) => {
  return (
    <div>
      <Formik
        initialValues={{
          manufacturer: "",
          model: "",
          year: "",
          vin: "",
          registration_plate: "",
          email: "",
        }}
        validationSchema={validationSchema}
        onSubmit={(values, { setSubmitting }) => {
          setSubmitting(true);
          try {
            createVehicle({ variables: { input: values } });
            handleClose();
          } catch (error) {
            return error;
          }
          setSubmitting(false);
        }}
      >
        {({ values, isSubmitting, setFieldValue }) => (
          <Form>
            <SelectManufacturer
              setFieldValue={setFieldValue}
            ></SelectManufacturer>
            <div style={{ marginTop: "10px" }}>
              <MyTextField placeholder="model" name="model" />
            </div>
            <div>
              <MyTextField placeholder="year" name="year" />
            </div>
            <div>
              <MyTextField placeholder="vin" name="vin" />
            </div>
            <div>
              <MyTextField
                placeholder="license plate"
                name="registration_plate"
              />
            </div>
            <div style={{ marginTop: "15px", marginBottom: "10px" }} >
              <CreateCustomer setFieldValue={setFieldValue}></CreateCustomer>
            </div>
            <Button disabled={isSubmitting} onClick={handleClose}>
              Cancel
            </Button>
            <Button disabled={isSubmitting} type="submit">
              Submit
            </Button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default CreateForm;
