import { useQuery } from "@apollo/client"
import React from "react";
import { RouteComponentProps } from "react-router";
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Customer from "./Customer";
import ReportVehicle from "./ReportVehicle";
import { GET_REPORT } from "../../GraphQL/Queries/reports";

const TAX_RATE = 0.07;

const useStyles = makeStyles({
  table: {
    minWidth: 700,
    maxWidth: 800,
    marginTop: '4vw',
    marginLeft: '4vw',
  },
});

function ccyFormat(num: number) {
  return `${num.toFixed(2)}`;
}

interface Row {
  id: number;
  name: string;
  price: number;
}

function subtotal(items: Row[]) {
  return items.map(({ price }) => price).reduce((sum, i) => sum + i, 0);
}

type TParams = { reportId: string };

const SingleReport = ({ history, match, location }: RouteComponentProps<TParams>) => {
  const { reportId } = match.params;
  console.log(reportId);
  const { error, loading, data } = useQuery(GET_REPORT, { variables: { id: +reportId } });
  const classes = useStyles();
  if (loading) {
    return <div>Loading!</div>
  }

  interface Service {
    name: string;
    price: number;
  }

  interface Report {
    id: number;
    customer: any;
    services: Service[];
    vehicle: any;
    newServices: string;
    date: string;
  }

  interface Reports {
    reports: Report[];
  }

  const newData = [];
  newData.push(data.report);

  const invoiceSubtotal = subtotal(data.report.services);
  const invoiceTaxes = TAX_RATE * invoiceSubtotal;
  const invoiceTotal = invoiceTaxes + invoiceSubtotal;

  return (
    <div>
      <Customer customer={data.report.customer}></Customer>
      <ReportVehicle vehicle={data.report.vehicle}></ReportVehicle>
      <TableContainer component={Paper} className={classes.table}>
        <Table aria-label="spanning table">
          <TableHead>
            <TableRow>
              <TableCell align="center" colSpan={1}>
                Services
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell align="right">Price</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {newData[0].services.map((row:any) => (
              <TableRow key={row.id}>
                <TableCell>{row.name}</TableCell>
                <TableCell align="right">{ccyFormat(row.price)}</TableCell>
              </TableRow>
            ))}
            <TableRow>
              <TableCell rowSpan={3} />
              <TableCell colSpan={2}>Subtotal</TableCell>
              <TableCell align="right">{ccyFormat(invoiceSubtotal)}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Tax</TableCell>
              <TableCell align="right">{`${(TAX_RATE * 100).toFixed(0)} %`}</TableCell>
              <TableCell align="right">{ccyFormat(invoiceTaxes)}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell colSpan={2}>Total</TableCell>
              <TableCell align="right">{ccyFormat(invoiceTotal)}</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}

export default SingleReport;