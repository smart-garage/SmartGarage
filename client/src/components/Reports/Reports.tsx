// do we need this file at all?

// import React from "react";
// import {
//   useQuery,
// } from "@apollo/client";
// import {
//   GridColDef,
//   GridRowsProp,
//   XGrid,
//   LicenseInfo,
//   useGridApiRef,
//   GridSelectionModelChangeParams,
// } from "@material-ui/x-grid";
// import {
//   createMuiTheme,
//   makeStyles,
//   Theme,
// } from "@material-ui/core";
// import _ from "lodash";
// import { NavLink } from "react-router-dom";
// import { REPORTS } from "../../GraphQL/Queries/reports";
// import { Report, ReportsData } from "../../utils/types/reports";

// LicenseInfo.setLicenseKey(
//   "b04c3d6853fe56a3fbf52d2a22210187T1JERVI6MjU2NTAsRVhQSVJZPTE2NTQ0OTY3MjYwMDAsS0VZVkVSU0lPTj0x"
// );

// const defaultTheme = createMuiTheme();
// const useStyles = makeStyles(
//   (theme: Theme) => ({
//     root: {
//       justifyContent: "center",
//       display: "flex",
//       borderBottom: `1px solid ${theme.palette.divider}`,
//     },
//   }),
//   { defaultTheme }
// );

// interface ToolbarProps {
//   selectedRow: GridSelectionModelChangeParams | null;
//   customerId: number;

// }

// function EditToolbar(props: ToolbarProps) {
//   const classes = useStyles();

//   const { selectedRow, customerId } = props;

//   const handleMouseDown = (event: React.SyntheticEvent) => {
//     // Keep the focus in the cell
//     event.preventDefault();
//   };

//   return (
//     <div className={classes.root}>
//       {selectedRow && <NavLink
//         to={`${customerId}/reports/${selectedRow?.selectionModel[0]}`}
//         onMouseDown={handleMouseDown}
//         color="primary"
//       >
//         View Detailed Report!
//       </NavLink>}
//     </div>
//   );
// }

// interface ReportsProps {
//   customerId: number;
// }

// const Reports = ({ customerId }: ReportsProps) => {
//   const { error, loading, data } = useQuery<ReportsData>(REPORTS, { variables: { customerId: 1 } });
//   const apiRef = useGridApiRef();

//   const [selectedRow, setSelectedRow] =
//     React.useState<GridSelectionModelChangeParams | null>(null);

//   const handleRowClick = React.useCallback(
//     (params: GridSelectionModelChangeParams) => {
//       setSelectedRow(params);
//     },
//     []
//   );

//   // Prevent from rolling back on escape
//   const handleCellKeyDown = React.useCallback(
//     (params, event: React.KeyboardEvent) => {
//       if (["Escape", "Delete", "Backspace", "Enter"].includes(event.key)) {
//         event.stopPropagation();
//       }
//     },
//     []
//   );

//   // Prevent from committing on blur
//   const handleCellBlur = React.useCallback(
//     (params, event?: React.SyntheticEvent) => {
//       if (params.cellMode === "edit") {
//         event?.stopPropagation();
//       }
//     },
//     []
//   );

//   if (loading) {
//     return <div>Loading...</div>;
//   }

//   const rowsData = data!.reports.map((report: Report) => {
//     const newReport = _.cloneDeep(report);
//     newReport.customer =
//       newReport.customer.firstName + ' ' + newReport.customer.lastName;
//     newReport.vehicle =
//       newReport.vehicle.manufacturer + ' ' + newReport.vehicle.model;
//     newReport.newServices = newReport.services.reduce((acc, service) => {
//       if (service === newReport.services[newReport.services.length - 1]) {
//         return acc += service.name
//       }
//       return acc += service.name + ', '
//     }, '')
//     newReport.date = newReport.date.split('T')[0];
//     return newReport;
//   });

//   const rows: GridRowsProp = rowsData;

//   const columns: GridColDef[] = [
//     { field: "id", headerName: "ID", width: 150 },
//     {
//       field: "customer",
//       headerName: "Customer",
//       width: 150,
//     },
//     { field: "vehicle", headerName: "Vehicle", width: 150 },
//     { field: "date", headerName: "Date", width: 180, type: 'date' },
//     { field: "newServices", headerName: "Services", width: 150 },
//   ];

//   return (
//     <div style={{ height: 520, maxWidth: "100%" }}>
//       <XGrid
//         disableMultipleSelection={true}
//         onSelectionModelChange={handleRowClick}
//         pagination={true}
//         pageSize={15}
//         columns={columns}
//         rows={rows}
//         loading={rows.length === 0}
//         rowHeight={38}
//         apiRef={apiRef}
//         onCellBlur={handleCellBlur}
//         onCellKeyDown={handleCellKeyDown}
//         components={{
//           Toolbar: EditToolbar,
//         }}
//         componentsProps={{
//           toolbar: {
//             selectedRow,
//             customerId
//           },
//         }}
//       />
//     </div>
//   );
// };

// export default Reports;
