import {  useQuery } from "@apollo/client";
import { RouteComponentProps } from "react-router-dom";
import { LOAD_CUSTOMER_BY_ID } from "../../GraphQL/Queries/customers";
import Reports from "./Reports";

type TParams = { id: string };

const DetailedSingleCustomer = ({ history, match, location }: RouteComponentProps<TParams>) => {
    const id = +match.params.id;
    const { error, loading, data, refetch } = useQuery(LOAD_CUSTOMER_BY_ID,
        { variables: { id } });

    return (
        <div>
            {data && (<div>
              
                <p><b>Username: </b>{data.customerById.email}</p>
                <p><b>First name: </b>{data.customerById.firstName}</p>
                <p><b>Last name: </b>{data.customerById.lastName}</p>
                <p><b>Phone: </b>{data.customerById.phone}</p>
                <Reports customerId={data.customerById.id}></Reports>
            </div>)}
        </div>
    )
}

export default DetailedSingleCustomer;
