import { useQuery } from "@apollo/client";
import { useContext } from "react";
import { RouteComponentProps } from "react-router-dom";
import { LOAD_CUSTOMER_SELF_BY_ID } from "../../GraphQL/Queries/customers";
import AuthContext from "../../providers/AuthContext";
import Report from "./Reports";

interface User {
    email: string,
    exp: number,
    iat: number,
    role: string,
    sub: number
}   

type TParams = { id: string };

const CustomerProfile = ({ history, match, location }: RouteComponentProps<TParams>) => {
    const auth = useContext(AuthContext);

    if (auth.authValue.user === undefined && auth.authValue.user === null) {
        history.push('/home');
    }

    const customer = auth.authValue.user;
    const loggedCustomerId = customer?.sub;
    const id = loggedCustomerId;

    const { error, loading, data, refetch } = useQuery(LOAD_CUSTOMER_SELF_BY_ID,
        { variables: { loggedCustomerId, id } });
    // const user = data.getSelfCustomerById;
    // console.log(data)

    return (
        <div>
            {data && (<div>
                <Report customerId={data.getSelfCustomerById.id}></Report>
            </div>)}
        </div>
    )

}

export default CustomerProfile;