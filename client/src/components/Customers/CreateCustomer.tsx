import { useMutation } from "@apollo/client";
import { REGISTER_CUSTOMER } from "../../GraphQL/Mutations/Customers-Mutations";
import { Formik, Form, FieldAttributes, useField } from "formik";
import { Button, TextField } from "@material-ui/core";
import * as Yup from "yup";

interface props {
  handleClose: () => void;
}

const phoneRegExp = /0[0-9]{9}$/;

const RegisterSchema = Yup.object().shape({
  email: Yup.string().email("Invalid email").required("Required"),
  firstName: Yup.string()
    .min(2, "Too Short!")
    .max(30, "Too Long!")
    .required("Required"),
  lastName: Yup.string()
    .min(2, "Too Short!")
    .max(30, "Too Long!")
    .required("Required"),
  phone: Yup.string()
    .required("Required")
    .matches(phoneRegExp, "Phone number is not valid"),
});

const MyTextField: React.FC<FieldAttributes<{}>> = ({
  placeholder,
  ...props
}) => {
  const [field, meta] = useField<{}>(props);
  const errorText = meta.error && meta.touched ? meta.error : "";
  return (
    <TextField
      placeholder={placeholder}
      {...field}
      helperText={errorText}
      error={!!errorText}
    />
  );
};

const CreateCustomer = ({ handleClose }: props) => {
  const [createCustomer, { error: mutationError }] =
    useMutation(REGISTER_CUSTOMER);

  return (
    <div>
      <Formik
        initialValues={{ email: "", firstName: "", lastName: "", phone: "" }}
        validationSchema={RegisterSchema}
        onSubmit={(values, { setSubmitting }) => {
          setSubmitting(true);
          try {
            createCustomer({
              variables: {
                email: values.email,
                firstName: values.firstName,
                lastName: values.lastName,
                phone: values.phone,
              },
            });
            handleClose();
          } catch (err) {
            console.log(err);
          }
          setSubmitting(false);
        }}
      >
        {({ isSubmitting }) => (
          <Form>
            <div>
              {" "}
              <MyTextField name="email" placeholder="Email" />
            </div>
            <div>
              {" "}
              <MyTextField name="firstName" placeholder="First Name" />
            </div>
            <div>
              {" "}
              <MyTextField name="lastName" placeholder="Last Name" />
            </div>
            <div>
              {" "}
              <MyTextField name="phone" placeholder="Phone" />
            </div>
            <div>
              <Button disabled={isSubmitting} onClick={handleClose}>
                Cancel
              </Button>
              <Button disabled={isSubmitting} type="submit">
                Register
              </Button>
              {mutationError && console.log(mutationError)}
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default CreateCustomer;
