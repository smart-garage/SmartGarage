import React from "react";
import { useMutation, useQuery } from "@apollo/client";
import { LOAD_CUSTOMERS } from "../../GraphQL/Queries/customers";
import CreateCustomer from "./CreateCustomer";
import {
  GridCellParams,
  GridColDef,
  GridRowsProp,
  GridSelectionModelChangeParams,
  XGrid,
} from "@material-ui/x-grid";
import { useGridApiRef } from "@material-ui/data-grid";
import { CustomersData } from "../../utils/types/customers";
import { Button, Dialog, DialogTitle, DialogContent } from "@material-ui/core";
import AddIcon from '@material-ui/icons/Add';
import EditToolBar from "./EditToolbar";
import { DELETE_CUSTOMER, UPDATE_CUSTOMER } from "../../GraphQL/Mutations/Customers-Mutations";

const AllCustomers = () => {
  const { error, loading, data, refetch } = useQuery<CustomersData>(LOAD_CUSTOMERS);
  const [updateCustomer] = useMutation(UPDATE_CUSTOMER, { onCompleted: () => refetch() });
  const [removeCustomer] = useMutation(DELETE_CUSTOMER, { onCompleted: () => refetch() });

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const apiRef = useGridApiRef();
  const [selectedCellParams, setSelectedCellParams] =
    React.useState<GridCellParams | null>(null);

  const handleCellClick = React.useCallback((params: GridCellParams) => {
    setSelectedCellParams(params);
  }, []);

  const [selectedRow, setSelectedRow] =
    React.useState<GridSelectionModelChangeParams | null>(null);

  const handleRowClick = React.useCallback(
    (params: GridSelectionModelChangeParams) => {
      setSelectedRow(params);
    },
    []
  );

  const handleDoubleCellClick = React.useCallback(
    (params: GridCellParams, event: React.SyntheticEvent) => {
      event.stopPropagation();
    },
    []
  );

  // Prevent from rolling back on escape
  const handleCellKeyDown = React.useCallback(
    (params, event: React.KeyboardEvent) => {
      if (["Escape", "Delete", "Backspace", "Enter"].includes(event.key)) {
        event.stopPropagation();
      }
    },
    []
  );

  // Prevent from committing on blur
  const handleCellBlur = React.useCallback(
    (params, event?: React.SyntheticEvent) => {
      if (params.cellMode === "edit") {
        event?.stopPropagation();
      }
    },
    []
  );

  if (loading) {
    return <div>Loading...</div>;
  }

  const columns: GridColDef[] = [
    {
      field: "firstName",
      headerName: "First Name",
      width: 150,
      editable: true,
    },
    {
      field: "lastName",
      headerName: "Last Name",
      width: 150,
      editable: true,
    },
    {
        field: "email",
        headerName: "Email",
        width: 150,
        editable: true,
      },
      {
        field: "phone",
        headerName: "Phone number",
        width: 150,
        editable: true,
      },
  ];
  const rows: GridRowsProp = data!.customers;

  if (error) return <p>Error: {error}</p>;

  return (
    <div style={{ height: 520, maxWidth: "100%" }}>
      <XGrid
        disableMultipleSelection={true}
        onSelectionModelChange={handleRowClick}
        pagination={true}
        pageSize={15}
        columns={columns}
        rows={rows}
        loading={rows.length === 0}
        rowHeight={38}
        apiRef={apiRef}
        onCellClick={handleCellClick}
        onCellDoubleClick={handleDoubleCellClick}
        onCellBlur={handleCellBlur}
        onCellKeyDown={handleCellKeyDown}
        components={{
          Toolbar: EditToolBar,
        }}
        componentsProps={{
          toolbar: {
            selectedCellParams,
            apiRef,
            setSelectedCellParams,
            selectedRow,
            updateCustomer,
            removeCustomer
          },
        }}
      />
      <Button
        variant="outlined"
        color="primary"
        onClick={handleClickOpen}
        startIcon={<AddIcon />}
      >
        Add Customer
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Register a new customer!</DialogTitle>
        <DialogContent>
          <CreateCustomer
            handleClose={handleClose}
          ></CreateCustomer>
        </DialogContent>
      </Dialog>
    </div>
  );
};

export default AllCustomers;
