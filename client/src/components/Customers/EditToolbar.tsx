import React from "react";
import {
  FetchResult,
  MutationFunctionOptions,
  OperationVariables,
} from "@apollo/client";
import * as Yup from "yup";
import DeleteIcon from "@material-ui/icons/Delete";
import {
  GridApiRef,
  GridSelectionModelChangeParams,
} from "@material-ui/x-grid";
import {
  Button,
  createMuiTheme,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  makeStyles,
  Snackbar,
  Theme,
} from "@material-ui/core";
import MuiAlert, { AlertProps } from "@material-ui/lab/Alert";
import EditIcon from "@material-ui/icons/Edit";
import SaveIcon from "@material-ui/icons/Save";
import ContactPhoneIcon from '@material-ui/icons/ContactPhone';
import { NavLink } from "react-router-dom";

function Alert(props: AlertProps) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const validationSchema = Yup.object().shape({
  email: Yup.string().required("Required!").email('Please enter a valid email!'),
  firstName: Yup.string().required("Required!"),
  lastName: Yup.string().required("Required!"),
  phone: Yup.string().required("Required!").length(10, 'Please enter a valid phone number!'),
});

const defaultTheme = createMuiTheme();
const useStyles = makeStyles(
  (theme: Theme) => ({
    root: {
      justifyContent: "center",
      display: "flex",
      borderBottom: `1px solid ${theme.palette.divider}`,
    },
  }),
  { defaultTheme }
);

interface EditToolbarProps {
  apiRef: GridApiRef;
  setSelectedCellParams: (value: any) => void;
  selectedCellParams: any;
  selectedRow: GridSelectionModelChangeParams | null;
  updateCustomer: any;
  removeCustomer: (
    options?: MutationFunctionOptions<any, OperationVariables> | undefined
  ) => Promise<FetchResult<any, Record<string, any>, Record<string, any>>>;
}

let snackBarError = "";

const EditToolBar = (props: EditToolbarProps) => {
  const {
    selectedCellParams,
    apiRef,
    setSelectedCellParams,
    updateCustomer,
    removeCustomer,
    selectedRow,
  } = props;
  const classes = useStyles();

  // pass isEmolyee as prop and if not, return null

  const [open, setOpen] = React.useState(false);
  const id = selectedRow?.selectionModel[0];

  const handleSnackBarClick = () => {
    setOpen(true);
  };

  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  const handleClick = () => {
    if (!selectedCellParams) {
      return;
    }
    const { id, field, cellMode } = selectedCellParams;
    const { email, firstName, lastName, phone } = selectedCellParams.row;
    if (cellMode === "edit") {
      const editedCellProps = apiRef.current.getEditCellPropsParams(id, field);
      apiRef.current.setCellMode(id, field, "view");
      const input = { email, firstName, lastName, phone };
      const fieldToEdit = selectedCellParams.field;
      //@ts-ignore
      input[fieldToEdit] = editedCellProps.props.value;
      // validation
      validationSchema
        .validate(input)
        .then(() => updateCustomer({ variables: { input, id } }))
        .then(() => apiRef.current.commitCellChange(editedCellProps))
        .catch((err) => {
          handleSnackBarClick();
          snackBarError = err.message;
        })
        .finally(() =>
          setSelectedCellParams({ ...selectedCellParams, cellMode: "view" })
        );
    } else {
      apiRef.current.setCellMode(id, field, "edit");
      setSelectedCellParams({ ...selectedCellParams, cellMode: "edit" });
    }
  };

  const handleMouseDown = (event: React.SyntheticEvent) => {
    // Keep the focus in the cell
    event.preventDefault();
  };

  // these handle the delete dialog
  const [deleteOpen, setDeleteOpen] = React.useState(false);

  const handleDeleteOpen = () => {
    setDeleteOpen(true);
  };

  const handleDeleteClose = () => {
    setDeleteOpen(false);
  };

  const handleDelete = (event: React.SyntheticEvent) => {
    event.preventDefault();

    handleDeleteClose();
    removeCustomer({ variables: { id } });
  };

  return (
    <div className={classes.root}>
      <Button
       variant="contained"
       color="primary"
        onClick={handleClick}
        onMouseDown={handleMouseDown}
        disabled={!selectedCellParams}
        startIcon={
          selectedCellParams?.cellMode === "edit" ? <SaveIcon /> : <EditIcon />
        }
      >
        {selectedCellParams?.cellMode === "edit" ? "Save" : "Edit"}
      </Button>
      <Button
        onClick={handleDeleteOpen}
        onMouseDown={handleMouseDown}
        variant="contained"
        color="secondary"
        startIcon={<DeleteIcon />}
      >
        DELETE
      </Button>
      {selectedCellParams && <Button startIcon={<ContactPhoneIcon />}>
        <NavLink to={`customers/${selectedCellParams.id}`}>View Detailed Profile!</NavLink>
      </Button>}
      <Dialog open={deleteOpen} onClose={handleDeleteClose}>
        <DialogTitle id="alert-dialog-title">
          {"Are you sure you want to delete these services?"}
        </DialogTitle>
        <DialogContent>
          {/* <DialogContentText id="alert-dialog-description">
            </DialogContentText> */}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDeleteClose} color="primary">
            No, go back!
          </Button>
          <Button onClick={handleDelete} color="primary" autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>

      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          {snackBarError}
        </Alert>
      </Snackbar>
    </div>
  );
};

export default EditToolBar;
