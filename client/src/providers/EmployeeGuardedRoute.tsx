import React, { useContext } from 'react';
import { Route, Redirect } from 'react-router-dom';
import AuthContext from './AuthContext';

const EmployeeGuardedRoute = ({ Component, ...rest }: any) => {
  const auth = useContext(AuthContext);

  const routeComponent = (props: any): any => {
    return <Component {...props} />
  }

  if (auth.authValue.user) {
    return (auth.authValue.isLoggedIn && auth.authValue.user.role === 'employee') ? <Route {...rest} render={routeComponent} /> : <Redirect to='/login/employee' />;
  } else {
    return <Redirect to='/login/employee' />;
  }
};

export default EmployeeGuardedRoute;
