import React, { useContext } from 'react';
import { Route, Redirect } from 'react-router-dom';
import AuthContext from './AuthContext';

const GuardedRoute = ({ Component, ...rest }: any) => {
  const auth = useContext(AuthContext);

  const routeComponent = (props: any): any => {
    return <Component {...props} />
  }
  return auth.authValue.isLoggedIn ? <Route {...rest} render={routeComponent} /> : <Redirect to='/login/customer' />;
};

export default GuardedRoute;
