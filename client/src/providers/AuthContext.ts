import { createContext } from 'react';
import jwtDecode from 'jwt-decode';

interface AuthProps {
    user: any,
    isLoggedIn: boolean
};

interface User {
    email: string,
    exp: number,
    iat: number,
    role: string,
    sub: number
}

const AuthContext = createContext<{ authValue: { user: User | null, isLoggedIn: boolean }, setAuthState: any }>({
    authValue: {
        isLoggedIn: false,
        user: null,
    },
    setAuthState: ({ user, isLoggedIn }: AuthProps) => { },
});

export const getUser = (): any => {
    try {
        return jwtDecode(localStorage.getItem('token') || '');
    } catch (error) {
        return null;
    }
}

export default AuthContext;
