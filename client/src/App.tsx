import React, {
  BrowserRouter,
  Route,
  Redirect,
  Switch,
} from "react-router-dom";
import "./App.css";
import { ApolloClient, ApolloProvider, createHttpLink, InMemoryCache } from "@apollo/client";
import AllCustomers from "./components/Customers/AllCustomers";
import DetailedSingleCustomer from "./components/Customers/DetailedSingleCustomer";
import Login from "./components/Auth/Login";
import AuthContext, { getUser } from "./providers/AuthContext";
import { useState } from "react";
import Navbar from "./components/Navigation/Navbar";
import Homepage from "./components/Homepage/Homepage";
import GuardedRoute from "./providers/GuardedRoute";
import { setContext } from '@apollo/client/link/context';
import Vehicle from "./components/Vehicles/Vehicle";
import AllVehicles from "./components/Vehicles/AllVehicles";
import Services from "./components/Services/Services";
import ForgottenPassword from "./components/Auth/ForgottenPassword";
import ForgottenPasswordRedirect from "./components/Auth/ForgottenPasswordRedirect";
import EmployeeGuardedRoute from "./providers/EmployeeGuardedRoute";
import CustomerProfile from "./components/Customers/CustomerProfile";
import SingleReport from "./components/Reports/SingleReport";
import NotFound from "./components/NotFound/NotFound";

const httpLink = createHttpLink({
  uri: 'http://localhost:5555/graphql',
});

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = localStorage.getItem('token');
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : "",
    }
  }
});

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache()
});

function App() {
  // can this be rewriten/omitted?
  const [authValue, setAuthState] = useState({
    user: getUser(),
    isLoggedIn: !!(getUser()),
  });

  return (
    <div className="App">
      <BrowserRouter>
        <AuthContext.Provider value={{ authValue, setAuthState }}>
        <Navbar />
        <ApolloProvider client={client}>
          <Switch>
            <Redirect path="/" exact to="/home" />
            <Route path="/home" component={Homepage} />
            <EmployeeGuardedRoute path="/vehicles/:id" component={Vehicle} />
            <EmployeeGuardedRoute path="/vehicles" component={AllVehicles} />
            <GuardedRoute path="/customers/:id/reports/:reportId" component={SingleReport} />
            <GuardedRoute path="/profile/reports/:reportId" component={SingleReport} />
            <GuardedRoute path="/profile" component={CustomerProfile} />
            <EmployeeGuardedRoute path="/services" component={Services} />
            {/* check if customers should be able to view servces and how */}
            <EmployeeGuardedRoute path="/customers/:id" component={DetailedSingleCustomer} />
            <EmployeeGuardedRoute path="/customers" component={AllCustomers} />
            {/* make login inaccessible when logged in */}
            <Route path="/login/customer" component={Login} />
            <Route path="/login/employee" component={Login} />
            <Route path="/forgotten-password-reset" component={ForgottenPasswordRedirect} />
            <Route path="/forgotten-password" component={ForgottenPassword} />
            <Route path="*" component={NotFound} />
          </Switch>
        </ApolloProvider>
        </AuthContext.Provider>
      </BrowserRouter>
    </div>
  );
}

export default App;
