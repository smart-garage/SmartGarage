export interface Service {
  id: number;
  name: string;
  price: number;
}

export interface ServicesData {
  services: Service[];
}