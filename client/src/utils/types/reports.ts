import { Service } from './service';

export interface Report {
  id: number;
  customer: any;
  services: Service[];
  vehicle: any;
  newServices: string;
  date: string;
}

export interface ReportsData {
  reports: Report[];
}
