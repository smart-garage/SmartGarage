export interface Vehicle {
  id: number;
  manufacturer: string;
  model: string
  year: string;
  vin: string;
  registration_plate: string;
  customer: any;
}

export interface Vehicles {
  vehicles: Vehicle[];
}