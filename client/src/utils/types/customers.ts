export interface Customers {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    phone: string;
  }
  
export interface CustomersData {
    customers: Customers[];
  }